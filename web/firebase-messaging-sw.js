importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js");

//Using singleton breaks instantiating messaging()
// App firebase = FirebaseWeb.instance.app;


const firebaseConfig = {
    apiKey: "AIzaSyCgqCs92H-qHsydK12T--PtHgalQQ4M2Ds",
  authDomain: "sunstone-7dea7.firebaseapp.com",
  projectId: "sunstone-7dea7",
  storageBucket: "sunstone-7dea7.appspot.com",
  messagingSenderId: "396373200932",
  appId: "1:396373200932:web:efba16ea11b55e371188b1",
  measurementId: "G-1K0X2L4PRJ"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
    const promiseChain = clients
        .matchAll({
            type: "window",
            includeUncontrolled: true
        })
        .then(windowClients => {
            for (let i = 0; i < windowClients.length; i++) {
                const windowClient = windowClients[i];
                windowClient.postMessage(payload);
            }
        })
        .then(() => {
            return registration.showNotification("New Message");
        });
    return promiseChain;
});
self.addEventListener('notificationclick', function(event) {
    console.log('notification received: ', event)
});