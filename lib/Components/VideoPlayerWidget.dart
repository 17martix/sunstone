import 'package:flutter/material.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWidget extends StatefulWidget {
  final String videoUrl;

  VideoPlayerWidget(this.videoUrl);

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState(videoUrl);
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  final VideoPlayerController videoPlayerController;
  final String videoUrl;
  double videoDuration = 0;
  double currentDuration = 0;
  _VideoPlayerWidgetState(this.videoUrl)
      : videoPlayerController = VideoPlayerController.network(videoUrl);

  @override
  void initState() {
    super.initState();
    videoPlayerController.initialize().then((_) {
      setState(() {
        videoDuration =
            videoPlayerController.value.duration.inMilliseconds.toDouble();
      });
    });

    videoPlayerController.addListener(() {
      setState(() {
        currentDuration =
            videoPlayerController.value.position.inMilliseconds.toDouble();
      });
    });
    print(videoUrl);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF737373),

      // This line set the transparent background
      child: Container(
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: SizeConfig.diagonal * 2),
              Container(
               color: Colors.white,
                constraints: BoxConstraints(
                    maxHeight: SizeConfig.safeBlockVertical * 30),
                child: videoPlayerController.value.isInitialized
                    ? AspectRatio(
                        aspectRatio: videoPlayerController.value.aspectRatio,
                        child: VideoPlayer(videoPlayerController),
                      )
                    : Container(
                        height: SizeConfig.safeBlockVertical * 30,
                    color: Colors.white,
                        child: Center(
                          child: CircularProgressIndicator(
                              color: Color(Styling.accentColor)),
                        ),
                      ),
              ),
              Slider(
                value: currentDuration,
                max: videoDuration,
                onChanged: (value) => videoPlayerController
                    .seekTo(Duration(milliseconds: value.toInt())),
                activeColor: Color(Styling.accentColor),
                inactiveColor: Color(Styling.primaryColor).withOpacity(0.4),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 24.0),
                child: InkWell(
                    child: CircleAvatar(
                      backgroundColor: Color(Styling.primaryColor),
                      radius: SizeConfig.diagonal * 2.5,
                      child: Icon(
                        videoPlayerController.value.isPlaying
                            ? Icons.pause
                            : Icons.play_arrow,
                        size: SizeConfig.diagonal * 2,
                      color: Colors.white,
                      ),
                    ),
                    onTap: () {
                      setState(() {
                        videoPlayerController.value.isPlaying
                            ? videoPlayerController.pause()
                            : videoPlayerController.play();
                      });
                    }),
              )
            ],
          )),
    );
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    super.dispose();
  }
}
