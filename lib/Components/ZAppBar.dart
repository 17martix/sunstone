import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Services/Authentication.dart';

import 'ZText.dart';

AppBar buildAppBar(
  context,
  Authentication auth,
  bool isBackAllowed,
  logout,
  backFunction,
  print,
  admin, {
  loadData,
  callStaff,
  homeDrawer,
}) {
  return AppBar(
    iconTheme: IconThemeData(color: Color(Styling.primaryColor)),
    backgroundColor: Colors.transparent,
    elevation: 0,
    leading: isBackAllowed
        ? IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(Styling.iconColor),
            ),
            onPressed: backFunction ??
                () {
                  Navigator.pop(context);
                })
        : homeDrawer == null
            ? null
            : IconButton(
                icon: Icon(
                  Icons.menu,
                  color: Color(Styling.iconColor),
                ),
                onPressed: homeDrawer),
    title: Center(
      child: RichText(
        text: TextSpan(
          style: Theme.of(context).textTheme.headline6!.copyWith(
                fontWeight: FontWeight.bold,
              ),
          children: <TextSpan>[
            TextSpan(
              text: 'Sun',
              style:
                  TextStyle(color: Color(Styling.primaryColor), fontSize: 20),
            ),
            TextSpan(
              text: 'stone',
              style: TextStyle(color: Color(Styling.accentColor), fontSize: 20),
            ),
          ],
        ),
      ),
    ),
    actions: [
      if (callStaff != null) loadCallButton(callStaff),
      if (loadData != null) loadDataButton(loadData),
      if (print != null) printButton(print),
      logout == null ? ZText(content: '') : logoutButton(logout),

      //if (admin != null) adminButton(admin),
    ],
  );
}

Widget loadCallButton(callStaff) {
  return IconButton(
    icon: Icon(
      Icons.call,
      color: Color(Styling.iconColor),
    ),
    onPressed: callStaff,
  );
}

Widget loadDataButton(loadData) {
  return IconButton(
    icon: Icon(
      Icons.upload,
      color: Color(Styling.iconColor),
    ),
    onPressed: loadData,
  );
}

Widget logoutButton(logout) {
  return IconButton(
    icon: Icon(
      FontAwesomeIcons.powerOff,
      color: Color(Styling.iconColor),
    ),
    onPressed: logout,
  );
}

Widget adminButton(admin) {
  return IconButton(
    icon: Icon(
      Icons.dashboard,
      color: Color(Styling.iconColor),
    ),
    onPressed: admin,
  );
}

Widget printButton(print) {
  return IconButton(
    icon: Icon(
      Icons.print,
      color: Color(Styling.iconColor),
    ),
    onPressed: print,
  );
}
