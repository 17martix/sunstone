import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:flutter/material.dart';
import 'package:sunstone/Helpers/Utils.dart';

import '../Helpers/Styling.dart';

class ZElevatedButton extends StatelessWidget {
  final onpressed;
  final child;
  final bottomPadding;
  final topPadding;
  final leftPadding;
  final rightPadding;
  final color;
  final height;
  final width;

  ZElevatedButton({
    required this.onpressed,
    required this.child,
    this.bottomPadding,
    this.leftPadding,
    this.rightPadding,
    this.topPadding,
    this.color,
    this.height,
    this.width,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return deviceSize(context) != DeviceSize.mobile
        ? largeScreenLayout()
        : new Padding(
            padding: EdgeInsets.fromLTRB(
                leftPadding ?? SizeConfig.diagonal * 1,
                topPadding ?? SizeConfig.diagonal * 1,
                rightPadding ?? SizeConfig.diagonal * 1,
                bottomPadding ?? SizeConfig.diagonal * 5),
            child: SizedBox(
              height: SizeConfig.diagonal * 6,
              width: double.infinity,
              child: new ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: color != null ? color : Color(Styling.primaryColor),
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                child: child,
                onPressed: onpressed,
              ),
            ));
  }

  Widget largeScreenLayout() {
    return SizedBox(
      height: height ?? 60,
      width: width ?? 170,
      child: new ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: color != null ? color : Color(Styling.primaryColor),
          elevation: 8.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
        child: child,
        onPressed: onpressed,
      ),
    );
  }
}
