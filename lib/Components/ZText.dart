import 'package:flutter/material.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Helpers/Utils.dart';

class ZText extends StatelessWidget {
  final String content;
  final Color? color;
  final double? fontSize;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final FontStyle? fontStyle;
  final TextAlign? textAlign;
  final TextDecoration? decoration;
  final int? maxLines;
  final double? height;

  ZText({
    required this.content,
    this.color,
    this.fontSize,
    this.fontWeight,
    this.overflow,
    this.fontStyle,
    this.textAlign,
    this.decoration,
    this.maxLines,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return deviceSize(context) != DeviceSize.mobile
        ? largeScreenLayout()
        : Text(
            content,
            overflow: overflow ?? TextOverflow.ellipsis,
            textAlign: textAlign ?? TextAlign.start,
            maxLines: maxLines ?? null,
            style: TextStyle(
              height: height ?? null,
              decoration: decoration ?? null,
              color: color ?? Color(Styling.textColor),
              fontSize: fontSize ,
              fontWeight: fontWeight ?? FontWeight.normal,
              fontStyle: fontStyle ?? FontStyle.normal,
            ),
          );
  }

  Widget largeScreenLayout() {
    return Text(
      content,
      overflow: overflow ?? TextOverflow.ellipsis,
      textAlign: textAlign ?? TextAlign.start,
      maxLines: maxLines ?? null,
      style: TextStyle(
        height: height ?? 0.0,
        decoration: decoration ?? null,
        color: color ?? Color(Styling.textColor),
        fontSize: fontSize,
        fontWeight: fontWeight ?? FontWeight.normal,
        fontStyle: fontStyle ?? FontStyle.normal,
      ),
    );
  }
}
