import 'package:flutter/material.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Services/Authentication.dart';
import 'package:sunstone/Services/Database.dart';

import '../Components/ZText.dart';
import '../i18n.dart';

class DisabledPage extends StatefulWidget {
  final Authentication auth;
  final Database db;
  final String userId;
  final String userRole;

  DisabledPage({
    required this.auth,
    required this.db,
    required this.userId,
    required this.userRole,
  });

  @override
  _DisabledPageState createState() => _DisabledPageState();
}

class _DisabledPageState extends State<DisabledPage> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      color: Color(Styling.primaryBackgroundColor).withOpacity(0.7),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: ZText(
            content: I18n.of(context).accountDisabled,
            textAlign: TextAlign.center,
           
            overflow: TextOverflow.visible,
          ),
        ),
      ),
    );
  }
}
