import 'dart:async';
import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Models/Chat.dart';
import 'package:sunstone/Models/Fields.dart';
import 'package:sunstone/Models/UserProfile.dart';
import 'package:sunstone/Pages/DashboardPage.dart';
import 'package:sunstone/Services/Authentication.dart';
import 'package:sunstone/Services/Database.dart';
import 'package:sunstone/Services/Messaging.dart';
import 'package:sunstone/i18n.dart';

import 'LoginPage.dart';

class SplashPage extends StatefulWidget {
  final Authentication auth;
  final Database db;
  final Messaging messaging;

  SplashPage({
    required this.auth,
    required this.db,
    required this.messaging,
  });

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();

    Timer(
      Duration(seconds: 2),
      isLoggedIn,
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      backgroundColor: Color(Styling.primaryBackgroundColor),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: SizeConfig.diagonal * 2),
            child: Center(
              child: logo(),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(SizeConfig.diagonal * 2),
            child: CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }

  Widget logo() {
    return Hero(
      tag: I18n.of(context).appTitle,
      child: Padding(
        padding: EdgeInsets.all(SizeConfig.diagonal * 2),
        child: CircleAvatar(
          radius: SizeConfig.diagonal * 10,
          backgroundColor: Colors.transparent,
          child: Image(
            image: AssetImage('assets/logo.png'),
          ),
        ),
      ),
    );
  }

  Future<void> isLoggedIn() async {
    widget.messaging.firebaseMessaging.requestPermission();

    //original code
    User? user = await widget.auth.getCurrentUser();
    if (user?.uid == null) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => LoginPage(
                  auth: widget.auth,
                  db: widget.db,
                  messaging: widget.messaging,
                )),
      );
    } else {
      UserProfile? userProfile = await widget.db.getUserProfile(user!.uid);

      await widget.db.getConfiguration().timeout(Duration(seconds: 180),
          onTimeout: () {
        widget.db.useImages = true;
        widget.db.callNumber = "69751595";
      });

      if (userProfile == null) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => LoginPage(
              auth: widget.auth,
              db: widget.db,
              user: user,
              messaging: widget.messaging,
            ),
          ),
        );
      } else {
        Chat? chat;
        if (userProfile.role == Fields.client) {
          chat = await widget.db.getChat(user.uid);
        }

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => DashboardPage(
              auth: widget.auth,
              db: widget.db,
              userId: user.uid,
              userRole: userProfile.role,
              messaging: widget.messaging,
              userName: userProfile.name,
              userProfile: userProfile,
              chat: chat,
            ),
          ),
        );
      }
    }
  }
}
