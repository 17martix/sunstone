import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:sunstone/Components/ZText.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Helpers/Utils.dart';
import 'package:sunstone/Models/Chat.dart';
import 'package:sunstone/Models/Fields.dart';
import 'package:sunstone/Models/Result.dart';
import 'package:sunstone/Pages/SingleChatPage.dart';
import 'package:sunstone/Services/Authentication.dart';
import 'package:sunstone/Services/Database.dart';
import 'package:intl/intl.dart';
import 'package:sunstone/i18n.dart';

class ChatPage extends StatefulWidget {
  final Authentication auth;
  final Database db;
  final String userId;
  final String userRole;
  final DateFormat formatter = DateFormat('dd/MM/yyyy HH:mm');

  ChatPage({
    Key? key,
    required this.auth,
    required this.db,
    required this.userId,
    required this.userRole,
  }) : super(key: key);

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> with WidgetsBindingObserver {
  var chats;
  var _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    chatQuery();
    try {
      widget.db
          .resendConversations(widget.userId)
          .timeout(Duration(seconds: 60), onTimeout: () {
        log("timeout trying to resend conversations");
      });
    } catch (e) {
      log("error trying to resend conversations");
      log(e.toString());
    }
  }

  void chatQuery() {
    chats = widget.db.databaseReference
        .collection(Fields.chats)
        .orderBy(Fields.updatedAt, descending: true);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: () async {
        if (EasyLoading.isShow) {
          return false;
        } else {
          return true;
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        body: body(),
      ),
    );
  }

  Widget body() {
    return SingleChildScrollView(
      child: chatsStream(),
    );
  }

  Widget chatTiles(Chat chat) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      secondaryActions: [
        SlideAction(
          onTap: () {
            deleteChat(chat);
          },
          child: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.delete,
               
                    color: Color(Styling.accentColor),
                  ),
                  Text(
                    I18n.of(context).delete,
                    style: TextStyle(color: Color(Styling.accentColor)),
                  )
                ],
              ),
            ),
            height: SizeConfig.diagonal * 10,
            decoration: BoxDecoration(
              color: Color(Styling.primaryColor),
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
            ),
          ),
        ),
        SlideAction(
          onTap: () {},
          child: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.cancel,
                 
                  ),
                  Text(I18n.of(context).cancel)
                ],
              ),
            ),
            height: SizeConfig.diagonal * 10,
            decoration: BoxDecoration(
              color: Color(Styling.grey),
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
            ),
          ),
        ),
      ],
      child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.diagonal * 0.5,
              vertical: SizeConfig.diagonal * 0.25),
          child: InkWell(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SingleChatPage(
                    chat: chat,
                    db: widget.db,
                    auth: widget.auth,
                    userRole: widget.userRole,
                    userId: widget.userId,
                    hasBackButton: true,
                  ),
                ),
              );
            },
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.circular(10)),
              elevation: 2,
              child: Padding(
                padding:
                    EdgeInsets.symmetric(vertical: SizeConfig.diagonal * 1),
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: SizeConfig.diagonal * 1),
                      child: CircleAvatar(
                        radius: SizeConfig.diagonal * 1.5,
                        backgroundColor: Color(Styling.primaryColor),
                        child: ZText(content: chat.customerName[0]),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.diagonal * 1.2),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            ZText(
                              content: chat.customerName,
                              textAlign: TextAlign.left,
                              color: Color(Styling.textColor),
                              fontWeight: FontWeight.w700,
                            ),
                            SizedBox(
                              height: SizeConfig.diagonal * 1,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                ZText(
                                  content:
                                      '${widget.formatter.format(chat.updatedAt!.toDate())}',
                                  color: Color(Styling.textColor),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    if (chat.adminUnreadCount > 0)
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.diagonal * 1),
                        child: CircleAvatar(
                          radius: SizeConfig.diagonal * 1,
                          backgroundColor: Color(Styling.accentColor),
                          child: ZText(
                            content: chat.adminUnreadCount > 99
                                ? "99+"
                                : "${chat.adminUnreadCount}",
                            fontSize: 12,
                            color: Colors.white,
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          )),
    );
  }

  void deleteChat(Chat chat) async {
    EasyLoading.show(status: I18n.of(context).loading);
    bool isOnline = await hasConnection();
    if (!isOnline) {
      EasyLoading.dismiss();
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: ZText(content: I18n.of(context).noInternet)));
    } else {
      try {
        Result result = await widget.db
            .deleteChat(chat, widget.userId)
            .timeout(Duration(seconds: 180), onTimeout: () {
          Result result =
              Result(isSuccess: false, message: Fields.operationTooLong);
          return result;
        });
        if (result.isSuccess) {
          EasyLoading.dismiss();
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: ZText(content: result.getMessage(context)),
            ),
          );
        } else {
          EasyLoading.dismiss();
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: ZText(content: result.getMessage(context)),
            ),
          );
        }
      } catch (e) {
        EasyLoading.dismiss();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: ZText(
              content: e.toString(),
              overflow: TextOverflow.visible,
            ),
          ),
        );
      }
    }
  }

  Widget chatsStream() {
    return StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
      stream: chats.snapshots(),
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> snapshot) {
        if (snapshot.data == null) {
          return Center(
            child: ZText(
              content: "",
            ),
          );
        }

        if (snapshot.data!.docs.isEmpty) {
          return SizedBox(
            width: double.infinity,
            height: SizeConfig.diagonal * 50,
            child: Center(
              child: ZText(
                content: I18n.of(context).chatPlaceHolder,
            
                color: Color(Styling.primaryColor),
                fontWeight: FontWeight.w700,
              ),
            ),
          );
        }

        return new Column(
          children: snapshot.data!.docs
              .map((DocumentSnapshot<Map<String, dynamic>> document) {
            Chat chat = Chat.buildObject(document);

            if (chat.customerId == widget.userId) {
              return Container();
            } else {
              return chatTiles(chat);
            }
          }).toList(),
        );
      },
    );
  }
}
