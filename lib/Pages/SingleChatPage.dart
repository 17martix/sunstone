import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:extended_image/extended_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:file_selector/file_selector.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flowder/flowder.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sunstone/Components/VideoPlayerWidget.dart';
import 'package:sunstone/Components/ZAppBar.dart';
import 'package:sunstone/Components/ZText.dart';
import 'package:sunstone/Components/ZTextField.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Helpers/Utils.dart';
import 'package:sunstone/Models/Chat.dart';
import 'package:sunstone/Models/Conversation.dart';
import 'package:sunstone/Models/Fields.dart';
import 'package:sunstone/Models/Folders.dart';
import 'package:sunstone/Services/Authentication.dart';
import 'package:sunstone/Services/Database.dart';
import 'package:collection/collection.dart';
import 'package:encrypt/encrypt.dart' as Encryption;
import 'package:sunstone/i18n.dart';
import 'package:url_launcher/url_launcher.dart';

class SingleChatPage extends StatefulWidget {
  final DateFormat formatter = DateFormat('dd/MM/yyyy  HH:mm');
  final Chat chat;
  final Database db;
  final String userId;
  final Authentication auth;
  final String userRole;
  final bool hasBackButton;

  SingleChatPage({
    Key? key,
    required this.chat,
    required this.db,
    required this.userId,
    required this.auth,
    required this.userRole,
    required this.hasBackButton,
  }) : super(key: key);

  @override
  State<SingleChatPage> createState() => _SingleChatPageState();
}

class _SingleChatPageState extends State<SingleChatPage>
    with WidgetsBindingObserver {
  Timestamp? lastSeenAt;

  TextEditingController messsageController = TextEditingController();
  GlobalKey<FormState> _messageKey = GlobalKey();

  String? message;
  List<Conversation> conversationList = [];
  String? newChatId;
  late bool isNewConversation;
  StreamSubscription<QuerySnapshot>? conversationStream;
  String? chatId;

  bool emojiShowing = false;
  var keyboardVisibilityController = KeyboardVisibilityController();

  @override
  void initState() {
    super.initState();
    isNewConversation = widget.chat.id == null ? true : false;
    chatId = widget.chat.id;
    newChatId = widget.chat.id;

    keyboardVisibilityController.onChange.listen((bool visible) {
      if (visible) {
        if (mounted)
          setState(() {
            emojiShowing = false;
          });
      }
    });

    if (widget.chat.id != null) {
      conversationQuery(widget.chat.id!);
    }

    if (widget.userRole == Fields.admin)
      widget.db.getLastSeenAt(widget.chat.customerId).then((value) {
        if (mounted) {
          setState(() {
            lastSeenAt = value;
          });
        }
      });
  }

  _onEmojiSelected(Emoji emoji) {
    messsageController
      ..text += emoji.emoji
      ..selection = TextSelection.fromPosition(
          TextPosition(offset: messsageController.text.length));
    message = messsageController.text;
  }

  _onBackspacePressed() {
    messsageController
      ..text = messsageController.text.characters.skipLast(1).toString()
      ..selection = TextSelection.fromPosition(
          TextPosition(offset: messsageController.text.length));

    message = messsageController.text;
  }

  Future<void> conversationQuery(String id) async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? notSentConersations;
    try {
      notSentConersations = prefs.getStringList(Fields.notSentConersations);
    } catch (e) {
      notSentConersations = null;
    }

    if (notSentConersations != null) {
      notSentConersations.forEach((element) {
        Conversation conversation = Conversation.buildObjectFromString(element);
        if (conversation.chatId == widget.chat.id) {
          conversationList.add(conversation);
        }
      });
    }

    conversationStream = widget.db.databaseReference
        .collection(Fields.chats)
        .doc(id)
        .collection(Fields.conversations)
        .orderBy(Fields.writtenAt, descending: true)
        .snapshots()
        .listen((snapshot) {
      log("inside");
      snapshot.docs.forEach((element) {
        Conversation conversation = Conversation.buildObject(element);
        String textConversation = conversation.buildStringFromObject();
        if (notSentConersations != null && notSentConersations.isNotEmpty) {
          notSentConersations.removeWhere((elt) => elt == textConversation);
        }

        conversationList.removeWhere((item) =>
            (conversation.localTime == item.localTime &&
                item.writtenAt == null));

        Conversation? exist = conversationList
            .firstWhereOrNull((item) => (conversation.id == item.id));
        if (exist == null) {
          conversationList.add(conversation);
        }
        if (mounted)
          setState(() {
            conversationList;
          });
      });

      if (notSentConersations != null && notSentConersations.isNotEmpty) {
        prefs.setStringList(Fields.notSentConersations, notSentConersations);
      } else
        prefs.remove(Fields.notSentConersations);
    });

    try {
      widget.db
          .resendConversations(widget.userId)
          .timeout(Duration(seconds: 60), onTimeout: () {
        log("timeout trying to resend conversations");
      });
    } catch (e) {
      log("error trying to resend conversations");
      log(e.toString());
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      appBar: widget.hasBackButton
          ? buildAppBar(
              context,
              widget.auth,
              true,
              null,
              null,
              null,
              null,
              /*   lastSeenAt == null
            ? null
            : '${I18n.lastSeenAt} : ${widget.formatter.format(lastSeenAt!.toDate())}',*/
            )
          : null,
      body: Column(
        children: [
          Container(
            child: Expanded(
              child: (conversationList.isEmpty && widget.chat.id != null)
                  ? Center(
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation(Color(Styling.accentColor)),
                      ),
                    )
                  : chatBoard(),
            ),
          ),
          Container(
            color: Color(Styling.grey),
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: SizeConfig.diagonal * 1),
              child: Row(
                children: [
                  Expanded(
                    child: Form(
                      key: _messageKey,
                      child: ZTextField(
                        controller: messsageController,
                        textInputAction: TextInputAction.newline,
                        //label: I18n.typeSomething,
                        hint: I18n.of(context).typeSomething,
                        onChanged: (newValue) {
                          if (mounted)
                            setState(() {
                              message = newValue;
                            });
                        },
                        validator: (value) => (value == null || value == '')
                            ? I18n.of(context).requiredInput
                            : null,
                        maxLines: 5,
                        outsideSuffix: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            IconButton(
                             
                              icon: Icon(Icons.attach_file),
                              color: Color(Styling.primaryColor),
                              onPressed: attachSomething,
                            ),
                            IconButton(
                           
                              icon: Icon(Icons.send),
                              color: Color(Styling.accentColor),
                              onPressed: () => sendMessage(
                                  I18n.of(context).updateMessage,
                                  Fields.encryptedText,
                                  null,
                                  null,
                                  message),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (!kIsWeb && (Platform.isAndroid || Platform.isIOS))
            Offstage(
              offstage: !emojiShowing,
              child: SizedBox(
                height: SizeConfig.safeBlockVertical * 30,
                child: EmojiPicker(
                    onEmojiSelected: (Category category, Emoji emoji) {
                      _onEmojiSelected(emoji);
                    },
                    onBackspacePressed: _onBackspacePressed,
                    config: Config(
                        columns: 7,
                        // Issue: https://github.com/flutter/flutter/issues/28894
                        emojiSizeMax:
                            32 * (!kIsWeb && Platform.isIOS ? 1.30 : 1.0),
                        verticalSpacing: 0,
                        horizontalSpacing: 0,
                        initCategory: Category.RECENT,
                        bgColor: const Color(0xFFF2F2F2),
                        indicatorColor: Color(Styling.primaryColor),
                        iconColor: Colors.grey,
                        iconColorSelected: Color(Styling.primaryColor),
                        progressIndicatorColor: Color(Styling.primaryColor),
                        backspaceColor: Color(Styling.primaryColor),
                        showRecentsTab: true,
                        recentsLimit: 28,
                        noRecents: ZText(content: I18n.of(context).noRecents),
                        tabIndicatorAnimDuration: kTabScrollDuration,
                        categoryIcons: const CategoryIcons(),
                        buttonMode: ButtonMode.MATERIAL)),
              ),
            ),
        ],
      ),
    );
  }

  void attachSomething() {
    FocusScope.of(context).requestFocus(new FocusNode());
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: Icon(
                    Icons.image,
                 
                    color: Color(Styling.primaryColor),
                  ),
                  title: ZText(
                    content: I18n.of(context).image,
                  ),
                  onTap: handleImageUpload,
                ),
                ListTile(
                  leading: Icon(
                    Icons.videocam,
                
                    color: Color(Styling.primaryColor),
                  ),
                  title: ZText(
                    content: I18n.of(context).video,
                  ),
                  onTap: handleVideoUpload,
                ),
                ListTile(
                    leading: Icon(
                      Icons.insert_drive_file,
                 
                      color: Color(Styling.primaryColor),
                    ),
                    title: ZText(
                      content: I18n.of(context).file,
                    ),
                    onTap: handleFileUpload),
              ],
            ),
          );
        });
  }

  Future<void> handleImageUpload() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    try {
      XFile? result = await ImagePicker().pickImage(
        imageQuality: 60,
        maxWidth: 800,
        source: ImageSource.gallery,
      );

      if (result != null) {
        EasyLoading.show(status: I18n.of(context).uploading);

        bool isOnline = await hasConnection();
        if (!isOnline) {
          EasyLoading.dismiss();
          Navigator.pop(context);
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: ZText(content: I18n.of(context).noInternet)));
        } else {
          final tempName = result.name;
          final ext = tempName.split('.').last;
          final name = "${DateTime.now().millisecondsSinceEpoch}.$ext";

          if (chatId == null || chatId == "") {
            DocumentReference newChatReference =
                widget.db.databaseReference.collection(Fields.chats).doc();
            widget.chat.id = newChatReference.id;
            chatId = newChatReference.id;
            try {
              await widget.db.addChat(
                  widget.userId, newChatReference, widget.chat, chatId!);
            } catch (e) {
              log(e.toString());
            }
          }
          Reference ref =
              widget.db.storage.ref("${Folders.chats}/$chatId/$name");
          Uint8List data = await result.readAsBytes();
          await ref.putData(data);
          final uri = await ref.getDownloadURL();
          EasyLoading.dismiss();
          Navigator.pop(context);
          sendMessage(I18n.of(context).updateMessage, Fields.image, uri, name,
              I18n.of(context).updateMessage);
        }
      }
    } catch (e) {
      EasyLoading.dismiss();
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: ZText(
            content: e.toString(),
            overflow: TextOverflow.visible,
          ),
        ),
      );
    }
  }

  Future<void> handleVideoUpload() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    try {
      final result = await ImagePicker().pickVideo(
        maxDuration: Duration(seconds: 30),
        source: ImageSource.gallery,
      );

      if (result != null) {
        EasyLoading.show(status: I18n.of(context).uploading);

        bool isOnline = await hasConnection();
        if (!isOnline) {
          EasyLoading.dismiss();
          Navigator.pop(context);
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: ZText(content: I18n.of(context).noInternet)));
        } else {
          final tempName = result.name;
          final ext = tempName.split('.').last;
          final name = "${DateTime.now().millisecondsSinceEpoch}.$ext";

          if (chatId == null || chatId == "") {
            DocumentReference newChatReference =
                widget.db.databaseReference.collection(Fields.chats).doc();
            widget.chat.id = newChatReference.id;
            chatId = newChatReference.id;
            try {
              await widget.db.addChat(
                  widget.userId, newChatReference, widget.chat, chatId!);
            } catch (e) {
              log(e.toString());
            }
          }
          Reference ref =
              widget.db.storage.ref("${Folders.chats}/$chatId/$name");
          Uint8List data = await result.readAsBytes();
          await ref.putData(data);
          final uri = await ref.getDownloadURL();
          EasyLoading.dismiss();
          Navigator.pop(context);
          sendMessage(I18n.of(context).updateMessage, Fields.video, uri, name,
              I18n.of(context).updateMessage);
        }
      }
    } catch (e) {
      EasyLoading.dismiss();
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: ZText(
            content: e.toString(),
            overflow: TextOverflow.visible,
          ),
        ),
      );
    }
  }

  Future<void> handleFileUpload() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    try {
      final result = await FilePicker.platform.pickFiles(
        type: FileType.any,
        allowCompression: true,
        withData: true,
      );

      if (result != null) {
        if (result.files.single.size > 5000000) {
          Navigator.pop(context);
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: ZText(content: I18n.of(context).fileBig)));
        } else {
          EasyLoading.show(status: I18n.of(context).uploading);

          bool isOnline = await hasConnection();
          if (!isOnline) {
            EasyLoading.dismiss();
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: ZText(content: I18n.of(context).noInternet)));
          } else {
            final ext = result.files.single.extension;
            final name = "${DateTime.now().millisecondsSinceEpoch}.$ext";

            if (chatId == null || chatId == "") {
              DocumentReference newChatReference =
                  widget.db.databaseReference.collection(Fields.chats).doc();
              widget.chat.id = newChatReference.id;
              chatId = newChatReference.id;
              try {
                await widget.db.addChat(
                    widget.userId, newChatReference, widget.chat, chatId!);
              } catch (e) {
                log(e.toString());
              }
            }
            Reference ref =
                widget.db.storage.ref("${Folders.chats}/$chatId/$name");
            Uint8List? data = await result.files.single.bytes;
            if (data != null) {
              await ref.putData(data);
              final uri = await ref.getDownloadURL();
              EasyLoading.dismiss();
              Navigator.pop(context);
              sendMessage(I18n.of(context).updateMessage, Fields.file, uri,
                  name, I18n.of(context).updateMessage);
            }
          }
        }
      }
    } catch (e) {
      EasyLoading.dismiss();
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: ZText(
            content: e.toString(),
            overflow: TextOverflow.visible,
          ),
        ),
      );
    }
  }

  /*void addEmoji() {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (mounted)
      setState(() {
        emojiShowing = !emojiShowing;
      });
  }*/

  void sendMessage(String? oldFormatMessage, String type, String? url,
      String? fileName, String? encryptedText) async {
    if (encryptedText != null &&
        encryptedText.isNotEmpty &&
        oldFormatMessage != null &&
        oldFormatMessage.isNotEmpty) {
      newChatId = 'nothing';

      bool isCustomer = false;
      String from;
      String to;
      if (widget.userId == widget.chat.customerId) {
        isCustomer = true;
        to = Fields.admin;
        from = widget.chat.customerId;
      } else {
        isCustomer = false;
        to = widget.chat.customerId;
        from = Fields.admin;
      }

      String tempIV = (widget.chat.customerId + Fields.admin).substring(0, 16);
      final key = Encryption.Key.fromUtf8(Fields.messageKey);
      final iv = Encryption.IV.fromUtf8(tempIV);
      final encrypter = Encryption.Encrypter(Encryption.AES(key));

      final String text = encrypter.encrypt(encryptedText, iv: iv).base64;
      encryptedText = '';
      if (mounted)
        setState(() {
          messsageController.clear();
        });

      if (chatId == null || chatId == "") {
        DocumentReference newChatReference =
            widget.db.databaseReference.collection(Fields.chats).doc();
        widget.chat.id = newChatReference.id;
        chatId = newChatReference.id;
        try {
          await widget.db
              .addChat(widget.userId, newChatReference, widget.chat, chatId!);
        } catch (e) {
          newChatId = null;
          log(e.toString());
        }
      }

      DocumentReference conversationDocument = widget.db.databaseReference
          .collection(Fields.chats)
          .doc(chatId)
          .collection(Fields.conversations)
          .doc();

      Conversation conversation = Conversation(
        id: conversationDocument.id,
        message: oldFormatMessage,
        writtenById: from,
        chatId: chatId,
        writtenToId: to,
        writtenAt: null,
        localTime: DateTime.now().millisecondsSinceEpoch,
        fileName: fileName,
        type: type,
        url: url,
        encryptedMessage: text,
      );

      if (mounted)
        setState(() {
          conversationList.add(conversation);
        });

      final prefs = await SharedPreferences.getInstance();
      List<String>? notSentConersations;
      try {
        notSentConersations = prefs.getStringList(Fields.notSentConersations);
      } catch (e) {
        notSentConersations = null;
      }

      if (notSentConersations == null) notSentConersations = [];
      notSentConersations.add(conversation.buildStringFromObject());
      prefs.setStringList(Fields.notSentConersations, notSentConersations);

      newChatId = chatId;

      try {
        conversation = await widget.db.addMessage(widget.chat, chatId!,
            conversation, isCustomer, conversationDocument);
      } catch (e) {
        if (mounted)
          setState(() {
            conversationList.remove(conversation);
          });
      }

      //newChatId = chatId;
      //chatId = conversation.chatId;

      if (isNewConversation && newChatId != null && newChatId != 'nothing') {
        isNewConversation = false;
        log("restating conv");
        if (mounted)
          setState(() {
            conversationQuery(newChatId!);
          });
      }
    }
  }

  Widget chatBoard() {
    conversationList.sort((a, b) {
      late int aValue;
      late int bValue;
      if (a.writtenAt == null) {
        aValue = a.localTime;
      } else {
        aValue = a.writtenAt!.millisecondsSinceEpoch;
      }

      if (b.writtenAt == null) {
        bValue = b.localTime;
      } else {
        bValue = b.writtenAt!.millisecondsSinceEpoch;
      }
      return bValue.compareTo(aValue);
    });
    return ListView(
      reverse: true,
      children: conversationList.map((Conversation conversation) {
        bool isCustomer;
        if (widget.userId == widget.chat.customerId)
          isCustomer = true;
        else
          isCustomer = false;

        widget.db.setUnreadCountToZero(widget.chat, isCustomer);

        return Padding(
          padding: EdgeInsets.all(SizeConfig.diagonal * 1.5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (conversation.writtenById == widget.userId ||
                  conversation.writtenById == widget.userRole)
                Expanded(flex: 1, child: Container()),
              Expanded(
                flex: 3,
                child: Container(
                  child: Wrap(
                    alignment: conversation.writtenById == widget.userId ||
                            conversation.writtenById == widget.userRole
                        ? WrapAlignment.end
                        : WrapAlignment.start,
                    children: [
                      Card(
                        elevation: 1.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            topLeft: conversation.writtenById ==
                                        widget.userId ||
                                    conversation.writtenById == widget.userRole
                                ? Radius.circular(10)
                                : Radius.zero,
                            topRight: conversation.writtenById ==
                                        widget.userId ||
                                    conversation.writtenById == widget.userRole
                                ? Radius.zero
                                : Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                        ),
                        child: Container(
                          decoration: conversation.writtenById ==
                                      widget.userId ||
                                  conversation.writtenById == widget.userRole
                              ? BoxDecoration(
                                  color: Color(Styling.primaryColor),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                )
                              : BoxDecoration(
                                  color: Color(Styling.grey),
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                ),
                          child: chatTextItem(conversation),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              if (conversation.writtenById != widget.userId &&
                  conversation.writtenById != widget.userRole)
                Expanded(
                  flex: 1,
                  child: Container(),
                ),
            ],
          ),
        );
      }).toList(),
    );
  }

  Widget chatTextItem(Conversation conversation) {
    String? text;
    if (conversation.encryptedMessage != null) {
      String tempIV = (widget.chat.customerId + Fields.admin).substring(0, 16);
      final key = Encryption.Key.fromUtf8(Fields.messageKey);
      final iv = Encryption.IV.fromUtf8(tempIV);
      final encrypter = Encryption.Encrypter(Encryption.AES(key));
      text = encrypter.decrypt64(conversation.encryptedMessage!, iv: iv);
    }
    return Padding(
      padding: EdgeInsets.all(SizeConfig.diagonal * 1.5),
      child: Column(
        crossAxisAlignment: conversation.writtenById == widget.userId ||
                conversation.writtenById == widget.userRole
            ? CrossAxisAlignment.end
            : CrossAxisAlignment.start,
        children: [
          if (conversation.type == null || conversation.type == Fields.text)
            Linkify(
              onOpen: (link) async {
                Uri? uri = Uri.tryParse(link.url);
                if (uri != null && await canLaunchUrl(uri)) {
                  await launchUrl(uri);
                }
              },
              text: '${conversation.message}',
              style: TextStyle(
                color: conversation.writtenById == widget.userId ||
                        conversation.writtenById == widget.userRole
                    ? Colors.white
                    : Color(Styling.textColor),
                fontWeight: FontWeight.normal,
                fontStyle: FontStyle.normal,
              ),
              linkStyle: TextStyle(
                color: Color(Styling.accentColor),
                fontWeight: FontWeight.normal,
                fontStyle: FontStyle.normal,
              ),
            ),
          if (conversation.type == Fields.encryptedText)
            Linkify(
              onOpen: (link) async {
                Uri? uri = Uri.tryParse(link.url);
                if (uri != null && await canLaunchUrl(uri)) {
                  await launchUrl(uri);
                }
              },
              text: '$text',
              style: TextStyle(
                color: conversation.writtenById == widget.userId ||
                        conversation.writtenById == widget.userRole
                    ? Colors.white
                    : Color(Styling.textColor),
                fontWeight: FontWeight.normal,
                fontStyle: FontStyle.normal,
              ),
              linkStyle: TextStyle(
                color: Color(Styling.accentColor),
                fontWeight: FontWeight.normal,
                fontStyle: FontStyle.normal,
              ),
            ),
          if (conversation.type == Fields.image)
            Container(
              height: SizeConfig.diagonal * 30,
              /* decoration: BoxDecoration(
                color: Color(Styling.primaryColor),
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                  image: FirebaseImage(
                    '${widget.db.firebaseBucket}/${Folders.chats}/${conversation.chatId}/${conversation.fileName}',
                    cacheRefreshStrategy: CacheRefreshStrategy.NEVER,
                  ),
                  fit: BoxFit.cover,
                ),
              ),*/
              child: ExtendedImage.network(
                conversation.url!,
                width: double.infinity,
                height: double.infinity,
                fit: BoxFit.cover,
                cache: true,
                cacheMaxAge: const Duration(hours: 720),
                loadStateChanged: imageStateHandler,
              ),
            ),
          if (conversation.type == Fields.video)
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Color(Styling.accentColor))),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    width: SizeConfig.diagonal * 1,
                  ),
                  Icon(
                    Icons.videocam,
                    color: conversation.writtenById == widget.userId ||
                            conversation.writtenById == widget.userRole
                        ? Colors.white
                        : Color(Styling.textColor),
               
                  ),
                  SizedBox(
                    width: SizeConfig.diagonal * 2,
                  ),
                  ZText(
                    content: I18n.of(context).video,
                    color: conversation.writtenById == widget.userId ||
                            conversation.writtenById == widget.userRole
                        ? Colors.white
                        : Color(Styling.textColor),
                  ),
                  SizedBox(
                    width: SizeConfig.diagonal * 2,
                  ),
                  Container(
                    //height: 40,
                    color: Color(Styling.accentColor),
                    child: IconButton(
                      icon: Icon(
                        Icons.play_arrow,
                        color: conversation.writtenById == widget.userId ||
                                conversation.writtenById == widget.userRole
                            ? Colors.white
                            : Color(Styling.textColor),
                       
                      ),
                      onPressed: () =>
                          showVideoPlayer(context, conversation.url!),
                    ),
                  ),
                ],
              ),
            ),
          if (conversation.type == Fields.file)
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Color(Styling.accentColor))),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    width: SizeConfig.diagonal * 1,
                  ),
                  Icon(
                    Icons.insert_drive_file,
                    color: conversation.writtenById == widget.userId ||
                            conversation.writtenById == widget.userRole
                        ? Colors.white
                        : Color(Styling.textColor),
                    
                  ),
                  SizedBox(
                    width: SizeConfig.diagonal * 2,
                  ),
                  ZText(
                    content: I18n.of(context).file,
                    color: conversation.writtenById == widget.userId ||
                            conversation.writtenById == widget.userRole
                        ? Colors.white
                        : Color(Styling.textColor),
                  ),
                  SizedBox(
                    width: SizeConfig.diagonal * 2,
                  ),
                  Container(
                    //height: 40,
                    color: Color(Styling.accentColor),
                    child: IconButton(
                      icon: Icon(
                        Icons.file_download,
                        color: conversation.writtenById == widget.userId ||
                                conversation.writtenById == widget.userRole
                            ? Colors.white
                            : Color(Styling.textColor),
            
                      ),
                      onPressed: () => downloadFile(
                          conversation.url!, conversation.fileName!),
                    ),
                  ),
                ],
              ),
            ),
          /*if(conversation.type==Fields.link)
          if(conversation.type==Fields.emoji)*/
          SizedBox(height: SizeConfig.diagonal * 1),
          conversation.writtenAt == null
              ? SizedBox(
                  width: SizeConfig.diagonal * 1.5,
                  height: SizeConfig.diagonal * 1.5,
                  child: CircularProgressIndicator(
                    strokeWidth: 1.0,
                    valueColor:
                        AlwaysStoppedAnimation(Color(Styling.accentColor)),
                  ),
                )
              : ZText(
                  content:
                      '${widget.formatter.format(conversation.writtenAt!.toDate())}',
                  color: conversation.writtenById == widget.userId ||
                          conversation.writtenById == widget.userRole
                      ? Colors.white
                      : Color(Styling.textColor),
                ),
        ],
      ),
    );
  }

  void showVideoPlayer(parentContext, String videoUrl) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: VideoPlayerWidget(videoUrl),
          );
        });
  }

  downloadFile(String fileUrl, String fileName) async {
    String? outputFile = '';
    if (!kIsWeb &&
        (Platform.isLinux || Platform.isWindows | Platform.isMacOS)) {
      outputFile = await FilePicker.platform.saveFile(
        dialogTitle: I18n.of(context).file,
        fileName: fileName,
        type: FileType.custom,
        allowedExtensions: ['zip'],
      );
    }

    try {
      if (kIsWeb) {
        String? path = await getSavePath();
        Uint8List? data = await widget.db.storage
            .ref("${Folders.chats}/$chatId/$fileName")
            .getData();
        if (path != null && data != null) {
          final file = XFile.fromData(data, name: fileName);
          await file.saveTo(path);
        }
      } else {
        String path;
        if (outputFile == null || outputFile.isEmpty) {
          //path = await _getDownloadPath(fileName);
          var status = await Permission.storage.status;
          if (!status.isGranted) {
            await Permission.storage.request();
            status = await Permission.storage.status;
          }

          if (status.isGranted) {
            Directory? directory;
            if (Platform.isIOS) {
              directory = await getApplicationDocumentsDirectory();
              print(directory.path);
            } else if (Platform.isAndroid) {
              directory = await getTemporaryDirectory();
            }

            if (directory == null) {
              throw Exception('Could not access local storage for '
                  'download. Please try again.');
            }
            path = "${directory.path}/$fileName";
            log('Temp cache save path: $path');
          } else {
            throw Exception('Storage Access has been denied.');
          }
        } else {
          path = outputFile;
        }

        final downloaderUtils = DownloaderUtils(
          /* progressCallback: (current, total) {
          final progress = (current / total) * 100;
          print('Downloading: $progress');
        },*/
          progressCallback: (current, total) {
            double progress = (current / total);
            //log('Downloading percentage is: $progress');
          },
          file: File(path),
          progress: ProgressImplementation(),
          onDone: () => print('Download done'),
          //deleteOnCancel: true,
        );
        /* String downloadURL = await _storage
            .ref()
            .child(BackupFields.backups)
            .child(fileName)
            .getDownloadURL();*/
        final core = await Flowder.download(fileUrl, downloaderUtils);

        if (Platform.isAndroid) {
          final params = SaveFileDialogParams(sourceFilePath: path);
          final filePath = await FlutterFileDialog.saveFile(params: params);

          print('Download path: $filePath');
        }
      }
    } catch (e) {
      rethrow;
    }
  }
}
