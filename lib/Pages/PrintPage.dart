import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:sunmi_printer_plus/enums.dart';
import 'package:sunmi_printer_plus/sunmi_printer_plus.dart';
import 'package:sunstone/Components/ZAppBar.dart';
import 'package:sunstone/Components/ZElevatedButton.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Utils.dart';
import 'package:sunstone/Models/Order.dart';
import 'package:sunstone/Services/Authentication.dart';
import 'package:intl/intl.dart';

import '../Components/ZText.dart';
import '../i18n.dart';

class PrintPage extends StatefulWidget {
  final Authentication auth;
  final String agentName;
  final String orderType;
  final String tableAddressLabel;
  final String tableAddress;
  final String? phoneNumber;
  final Order order;

  const PrintPage({
    Key? key,
    required this.auth,
    required this.agentName,
    required this.orderType,
    required this.tableAddressLabel,
    required this.tableAddress,
    required this.phoneNumber,
    required this.order,
  }) : super(key: key);

  @override
  State<PrintPage> createState() => _PrintPageState();
}

class _PrintPageState extends State<PrintPage> {
  bool printBinded = false;
  int paperSize = 0;
  String serialNumber = "";
  String printerVersion = "";

  @override
  void initState() {
    super.initState();

    _bindingPrinter().then((bool? isBind) async {
      SunmiPrinter.paperSize().then((int size) {
        if (mounted)
          setState(() {
            paperSize = size;
          });
      });

      SunmiPrinter.printerVersion().then((String version) {
        if (mounted)
          setState(() {
            printerVersion = version;
          });
      });

      SunmiPrinter.serialNumber().then((String serial) {
        if (mounted)
          setState(() {
            serialNumber = serial;
          });
      });

      if (mounted)
        setState(() {
          printBinded = isBind!;
        });
    });
  }

  Future<bool?> _bindingPrinter() async {
    final bool? result = await SunmiPrinter.bindingPrinter();
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, widget.auth, true, null, null, null, null),
      body: body(),
    );
  }

  Widget body() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 10,
            ),
            child: Text(
                "${I18n.of(context).printBinded}: " + printBinded.toString()),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 2.0),
            child:
                Text("${I18n.of(context).paperSize}: " + paperSize.toString()),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 2.0),
            child: Text("${I18n.of(context).serialNumber}: " + serialNumber),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 2.0),
            child: Text("${I18n.of(context).printVersion}: " + printerVersion),
          ),
          const Divider(),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.diagonal * 2),
            child: Row(children: [
              Expanded(
                  child: ZElevatedButton(
                      onpressed: printReceipt,
                      child: ZText(
                        content: "${I18n.of(context).printReceipt}",
                        color: Colors.white,
                      ))),
              /* Expanded(
                  child: ZElevatedButton(
                      onpressed: printLabel,
                      child: ZText(
                        content: "${I18n.of(context).printLabel}",
                        color: Colors.white,
                      ))),*/
            ]),
          )
        ],
      ),
    );
  }

  void printReceipt() async {
    DateTime dateTime = DateTime.now();
    final DateFormat formatDate = DateFormat('dd/MM/yy');
    final DateFormat formatTime = DateFormat('HH:mm');

    await SunmiPrinter.startTransactionPrint(true);
    await SunmiPrinter.setAlignment(SunmiPrintAlign.CENTER);
    await SunmiPrinter.printText(I18n.of(context).appTitle);
    await SunmiPrinter.lineWrap(1);
    await SunmiPrinter.setAlignment(SunmiPrintAlign.CENTER);
    await SunmiPrinter.printText(I18n.of(context).receipt);
    await SunmiPrinter.lineWrap(2);
    await SunmiPrinter.setAlignment(SunmiPrintAlign.CENTER);
    await SunmiPrinter.printText(formatDate.format(dateTime));
    await SunmiPrinter.lineWrap(2);
    await SunmiPrinter.printText(
        "${I18n.of(context).agentName} : ${widget.agentName}");
    await SunmiPrinter.printText(
        "${I18n.of(context).orderType} : ${widget.orderType}");
    await SunmiPrinter.printText(
        "${widget.tableAddressLabel} : ${widget.tableAddress}");
    await SunmiPrinter.printText(
        "${I18n.of(context).fone} : ${widget.phoneNumber}");
    await SunmiPrinter.lineWrap(2);
    for (int i = 0; i < widget.order.clientOrder.length; i++) {
      await SunmiPrinter.printText(
          "${widget.order.clientOrder[i].menuItem.name} x ${widget.order.clientOrder[i].count} : ${formatNumber(widget.order.clientOrder[i].menuItem.price)} x ${widget.order.clientOrder[i].count} = ${formatNumber(widget.order.clientOrder[i].menuItem.price * widget.order.clientOrder[i].count)} FBU");
    }
    await SunmiPrinter.lineWrap(1);
    await SunmiPrinter.printText(
        "${I18n.of(context).taxCharge} : ${appliedTaxFromTotal(context, widget.order.total, widget.order.taxPercentage)}");
    await SunmiPrinter.printText(
        "${I18n.of(context).grandTotal} : ${formatNumber(widget.order.grandTotal)} FBU");
    await SunmiPrinter.lineWrap(2);
    await SunmiPrinter.printText("${I18n.of(context).thankYou}");
    await SunmiPrinter.lineWrap(1);
    await SunmiPrinter.submitTransactionPrint();
    await SunmiPrinter.exitTransactionPrint(true);
  }

  void printLabel() async {}
}

/*class PrintPage extends StatefulWidget {
  final Authentication auth;
  final String agentName;
  final String orderType;
  final String tableAddressLabel;
  final String tableAddress;
  final String? phoneNumber;
  final Order order;

  PrintPage({
    required this.auth,
    required this.agentName,
    required this.orderType,
    required this.tableAddressLabel,
    required this.tableAddress,
    required this.phoneNumber,
    required this.order,
  });

  @override
  _PrintPageState createState() => _PrintPageState();
}

class _PrintPageState extends State<PrintPage> {
  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;

  List<BluetoothDevice> _devices = [];
  BluetoothDevice? _device;
  bool _connected = false;
  //String pathImage;
  Receipt receipt = Receipt();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    bool? isConnected = await bluetooth.isConnected;
    List<BluetoothDevice> devices = [];
    try {
      devices = await bluetooth.getBondedDevices();
    } on PlatformException {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: ZText(content: I18n.of(context).operationFailed)));
    }

    bluetooth.onStateChanged().listen((state) {
      switch (state) {
        case BlueThermalPrinter.CONNECTED:
          if (mounted)
            setState(() {
              _connected = true;
            });
          break;
        case BlueThermalPrinter.DISCONNECTED:
          if (mounted)
            setState(() {
              _connected = false;
            });
          break;
        default:
          print(state);
          break;
      }
    });

    if (!mounted) return;
    setState(() {
      _devices = devices;
    });

    if (isConnected != null && isConnected == true) {
      if (mounted)
        setState(() {
          _connected = true;
        });
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: buildAppBar(
        context,
        widget.auth,
        true,
        null,
        null,
        null,
        null,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Card(
            color: Colors.white.withOpacity(0.7),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(SizeConfig.diagonal * 1.5)),
            elevation: 16,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(SizeConfig.diagonal * 1),
                  child: ZText(
                    content: I18n.of(context).print,
                    fontWeight: FontWeight.bold,
                    fontSize: SizeConfig.diagonal * 1.5,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.diagonal * 1,
                      vertical: SizeConfig.diagonal * 0.5),
                  child: Divider(height: 2.0, color: Colors.black),
                ),
                SizedBox(
                  height: SizeConfig.diagonal * 2.5,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      width: SizeConfig.diagonal * 1,
                    ),
                    ZText(
                      content: I18n.of(context).device,
                      fontWeight: FontWeight.bold,
                    ),
                    SizedBox(
                      width: SizeConfig.diagonal * 1,
                    ),
                    Expanded(
                      child: DropdownButton(
                        items: _getDeviceItems(),
                        onChanged: (BluetoothDevice? value) {if (mounted)
                          setState(() => _device = value);
                        },
                        value: _device,
                     
                        style: TextStyle(
                          fontSize: SizeConfig.diagonal * 1.5,
                          color: Color(Styling.textColor),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig.diagonal * 2.5,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                      child: ZElevatedButton(
                        onpressed: () {
                          initPlatformState();
                        },
                        // bottomPadding: SizeConfig.diagonal * 1,
                        child: ZText(
                          content: I18n.of(context).refresh,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: SizeConfig.diagonal * 1,
                    ),
                    Expanded(
                      child: ZElevatedButton(
                        // bottomPadding: SizeConfig.diagonal * 1,
                        color: _connected
                            ? Color(Styling.accentColor)
                            : Color(Styling.primaryColor),
                        onpressed: _connected ? _disconnect : _connect,
                        child: ZText(
                          content: _connected
                              ? I18n.of(context).disconnect
                              : I18n.of(context).connect,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
                ZElevatedButton(
                  // bottomPadding: SizeConfig.diagonal * 1,
                  onpressed: () {
                    receipt.sample(
                        context,
                        widget.agentName,
                        widget.orderType,
                        widget.tableAddressLabel,
                        widget.tableAddress,
                        widget.phoneNumber,
                        widget.order);
                    //Navigator.of(context).pop();
                  },
                  child: ZText(
                    content: I18n.of(context).print,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<DropdownMenuItem<BluetoothDevice>> _getDeviceItems() {
    List<DropdownMenuItem<BluetoothDevice>> items = [];
    if (_devices.isEmpty) {
      items.add(DropdownMenuItem(
        child: ZText(content: I18n.of(context).none),
      ));
    } else {
      _devices.forEach((device) {
        items.add(DropdownMenuItem(
          child: ZText(content: device.name!),
          value: device,
        ));
      });
    }
    return items;
  }

  void _connect() {
    if (_device == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        new SnackBar(
          content: ZText(
            content: I18n.of(context).noDevices,
            color: Colors.white,
          ),
        ),
      );
      //show(I18n.of(context).noDevices);
    } else {
      bluetooth.isConnected.then((isConnected) {
        if (!isConnected!) {
          bluetooth.connect(_device!).catchError((error) {if (mounted)
            setState(() => _connected = false);
          });if (mounted)
          setState(() => _connected = true);
        }
      });
    }
  }

  void _disconnect() {
    bluetooth.disconnect();if (mounted)
    setState(() => _connected = true);
  }

  Future show(
    String message, {
    Duration duration: const Duration(seconds: 3),
  }) async {
    await new Future.delayed(new Duration(milliseconds: 100));
    ScaffoldMessenger.of(context).showSnackBar(
      new SnackBar(
        content: ZText(
          content: message,
          color: Colors.white,
        ),
        duration: duration,
      ),
    );
  }
}*/
