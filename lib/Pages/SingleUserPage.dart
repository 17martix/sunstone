import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:sunstone/Components/ZAppBar.dart';
import 'package:sunstone/Components/ZElevatedButton.dart';
import 'package:sunstone/Components/ZFlatButton.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Helpers/Utils.dart';
import 'package:sunstone/Models/Fields.dart';
import 'package:sunstone/Models/Room.dart';
import 'package:sunstone/Models/Statistic.dart';
import 'package:sunstone/Models/UserProfile.dart';
import 'package:sunstone/Services/Authentication.dart';
import 'package:sunstone/Services/Database.dart';
import 'package:intl/intl.dart';

import '../Components/ZText.dart';
import '../i18n.dart';

class SingleUserPage extends StatefulWidget {
  final Authentication auth;
  final Database db;
  final String userId;
  final DateFormat formatter = DateFormat('dd/MM/yy hh:mm');
  final DateFormat format = DateFormat('dd/MM');
  final String userRole;
  final UserProfile userData;

  SingleUserPage({
    required this.auth,
    required this.db,
    required this.userId,
    required this.userRole,
    required this.userData,
  });
  @override
  _SingleUserPageState createState() => _SingleUserPageState();
}

class _SingleUserPageState extends State<SingleUserPage> {
  var oneUserDetails;

  List<DocumentSnapshot<Map<String, dynamic>>> items = [];
  ScrollController _scrollController = ScrollController();
  bool isLoading = false;
  bool hasMore = true;

  QuerySnapshot<Map<String, dynamic>>? statref;
  DocumentSnapshot<Map<String, dynamic>>? lastDocument;

  List<BarChartGroupData> rawBarGroups = [];
  List<BarChartGroupData> showingBarGroups = [];
  int? touchedGroupIndex;

  final Color rightBarColor = Color(Styling.accentColor);
  final double width = 7;

  int documentLimit = 10;
  num maxY = 1;
  ScrollController _controller = ScrollController();
  late String newRole;
  List<String> roleList = [];

  // List<String> roomList = [];

  late String selectedRoom;
  var roomsStream = FirebaseFirestore.instance
      .collection(Fields.rooms)
      .where(Fields.isOccupied, isEqualTo: false);

  @override
  void initState() {
    super.initState();

    roleList = [
      Fields.admin,
      Fields.chef,
      Fields.client,
      Fields.chefBoissons,
      Fields.chefCuisine,
    ];
    newRole = Fields.client;

    oneUserDetails = FirebaseFirestore.instance
        .collection(Fields.users)
        .doc(widget.userData.id);

    statQuery();
    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      double delta = MediaQuery.of(context).size.width * 0.20;
      if (maxScroll - currentScroll <= delta) {
        statQuery();
      }
    });
  }

  void graphData() {
    int length = items.length;
    if (length > 7) length = 7;

    final List<BarChartGroupData> barItems = [];
    for (int i = 0; i < length; i++) {
      final barGroup = makeGroupData(i, items[i].data()![Fields.total]);
      barItems.add(barGroup);
      if (maxY < items[i].data()![Fields.total]) {
        if (mounted) {
          setState(() {
            maxY = items[i].data()![Fields.total];
          });
        }
      }
    }

    rawBarGroups = barItems;

    showingBarGroups = rawBarGroups;
  }

  void statQuery() async {
    if (isLoading) {
      return;
    }
    if (mounted) {
      setState(() {
        isLoading = true;
      });
    }

    if (lastDocument == null) {
      statref = await widget.db.databaseReference
          .collection(Fields.statisticUser)
          .where(Fields.userId, isEqualTo: widget.userData.id)
          .limit(documentLimit)
          .orderBy(Fields.date, descending: true)
          .get();
    } else {
      statref = await widget.db.databaseReference
          .collection(Fields.statisticUser)
          .where(Fields.userId, isEqualTo: widget.userData.id)
          .limit(documentLimit)
          .orderBy(Fields.date, descending: true)
          .startAfterDocument(lastDocument!)
          .get();
    }

    if (statref!.docs.length < documentLimit) {
      hasMore = false;
    }

    if (statref!.docs.length > 0)
      lastDocument = statref!.docs[statref!.docs.length - 1];
    if (mounted) {
      setState(() {
        for (int i = 0; i < statref!.docs.length; i++) {
          items.add(statref!.docs[i]);
        }
        graphData();
        isLoading = false;
      });
    }
  }

  void _actionPression(UserProfile userProfile) async {
    bool isActive = !userProfile.isActive;

    await widget.db.updateIsActive(userProfile.id!, isActive);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        context,
        widget.auth,
        true,
        null,
        null,
        null,
        null,
      ),
      body: body(),
      backgroundColor: Colors.transparent,
    );
  }

  Widget body() {
    return Column(children: [
      statList(),
      Expanded(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          controller: _controller,
          child: Column(
            children: [
              graph(),
              userStream(),
            ],
          ),
        ),
      ),
    ]);
  }

  Widget itemCard(Statistic statistic) {
    return Container(
      height: SizeConfig.diagonal * 10,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 8,
        color: Colors.white.withOpacity(0.7),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(SizeConfig.diagonal * 1.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ZText(
                      content: formatNumber(statistic.total),
                      color: Color(Styling.accentColor),
                    ),
                    ZText(
                      content: " Fbu",
                      color: Color(Styling.iconColor),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(SizeConfig.diagonal * 1.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ZText(
                      content: I18n.of(context).date,
                      textAlign: TextAlign.center,
                      color: Color(Styling.iconColor),
                    ),
                    ZText(
                      content:
                          "  : ${widget.formatter.format(statistic.date.toDate())}",
                      color: Color(Styling.iconColor),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget statList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.all(SizeConfig.diagonal * 0.5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ZText(
                content: I18n.of(context).dailyTotal,
                textAlign: TextAlign.center,
                color: Color(Styling.iconColor),
                fontWeight: FontWeight.bold,
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(
              horizontal: SizeConfig.diagonal * 2,
              vertical: SizeConfig.diagonal * 1),
          width: double.infinity,
          height: 1,
          color: Color(Styling.primaryColor),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          controller: _scrollController,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              items.length == 0
                  ? Center(
                      child: ZText(content: ""),
                    )
                  : Row(
                      children: items.map((document) {
                        Statistic statisticUser =
                            Statistic.buildObject(document);

                        return itemCard(statisticUser);
                      }).toList(),
                    ),
              isLoading
                  ? Container(
                      width: SizeConfig.diagonal * 8,
                      height: SizeConfig.diagonal * 8,
                      padding: EdgeInsets.all(SizeConfig.diagonal * 1.5),
                      child: Card(
                        elevation: 2,
                        color: Colors.white.withOpacity(0.7),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: CircularProgressIndicator(
                          semanticsLabel: 'Linear progress indicator',
                          backgroundColor: Color(Styling.primaryDarkColor),
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ],
    );
  }

  BarChartGroupData makeGroupData(
    int x,
    num y1,
  ) {
    return BarChartGroupData(barsSpace: 4, x: x, barRods: [
      BarChartRodData(
        y: y1.toDouble(),
        colors: [rightBarColor],
        width: 3,
      ),
    ]);
  }

  Widget graph() {
    return Container(
      child: AspectRatio(
        aspectRatio: 1.6,
        child: Card(
          elevation: 8,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          color: Color(Styling.primaryBackgroundColor).withOpacity(0.7),
          child: Padding(
            padding: EdgeInsets.all(SizeConfig.diagonal * 1),
            child: Column(
              //crossAxisAlignment: CrossAxisAlignment.stretch,
              //mainAxisAlignment: MainAxisAlignment.start,
              //mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Row(
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  //mainAxisSize: MainAxisSize.min,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    makeTransactionsIcon(),
                    SizedBox(width: SizeConfig.diagonal * 1),
                    ZText(
                      content: I18n.of(context).weeklyinventory,
                      color: Color(Styling.iconColor),
                      fontWeight: FontWeight.bold,
                    ),
                    SizedBox(width: SizeConfig.diagonal * 1),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: SizeConfig.diagonal * 1,
                      vertical: SizeConfig.diagonal * 1),
                  width: double.infinity,
                  height: 1,
                  color: Color(Styling.primaryColor),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: BarChart(
                      BarChartData(
                        maxY: maxY.toDouble(),
                        titlesData: FlTitlesData(
                          show: true,
                          bottomTitles: SideTitles(
                            showTitles: true,
                            getTextStyles: (context, value) => const TextStyle(
                                color: Color(Styling.iconColor), fontSize: 10),
                            margin: 15,
                            reservedSize: 8,
                            getTitles: (double value) {
                              switch (value.toInt()) {
                                case 0:
                                  Timestamp date =
                                      items[0].data()![Fields.date];
                                  return widget.format.format(date.toDate());
                                case 1:
                                  Timestamp date =
                                      items[1].data()![Fields.date];
                                  return widget.format.format(date.toDate());
                                case 2:
                                  Timestamp date =
                                      items[2].data()![Fields.date];
                                  return widget.format.format(date.toDate());
                                case 3:
                                  Timestamp date =
                                      items[3].data()![Fields.date];
                                  return widget.format.format(date.toDate());
                                case 4:
                                  Timestamp date =
                                      items[4].data()![Fields.date];
                                  return widget.format.format(date.toDate());
                                case 5:
                                  Timestamp date =
                                      items[5].data()![Fields.date];
                                  return widget.format.format(date.toDate());
                                case 6:
                                  Timestamp date =
                                      items[6].data()![Fields.date];
                                  return widget.format.format(date.toDate());

                                default:
                                  return '';
                              }
                            },
                          ),
                          leftTitles: SideTitles(
                            showTitles: true,
                            getTextStyles: (context, value) => const TextStyle(
                              color: Color(Styling.iconColor),
                              fontSize: 10,
                            ),
                            margin: 20,
                            reservedSize: 26,
                            interval: maxY / 4,
                            getTitles: (value) {
                              if (value == 0) {
                                return '0';
                              } else if (value == maxY / 4) {
                                return formatInterVal(maxY / 4)!;
                              } else if (value == maxY / 2) {
                                return formatInterVal(maxY / 2)!;
                              } else if (value == maxY * 3 / 4) {
                                return formatInterVal(maxY * 3 / 4)!;
                              } else if (value == maxY) {
                                return formatInterVal(maxY.toDouble())!;
                              } else {
                                return '';
                              }
                            },
                          ),
                        ),
                        borderData: FlBorderData(
                          show: false,
                        ),
                        barGroups: showingBarGroups,
                      ),
                      //swapAnimationDuration: Duration(milliseconds: 150),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget makeTransactionsIcon() {
    const double width = 7;
    const double space = 7;
    return Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: width,
            height: 10,
            color: Color(Styling.iconColor).withOpacity(0.4),
          ),
          const SizedBox(
            width: space,
          ),
          Container(
            width: width,
            height: 10,
            color: Color(Styling.iconColor).withOpacity(0.6),
          ),
          const SizedBox(
            width: space,
          ),
          Container(
            width: width,
            height: 10,
            color: Color(Styling.iconColor).withOpacity(0.9),
          ),
          const SizedBox(
            width: space,
          ),
        ]);
  }

  Widget userStream() {
    return StreamBuilder<DocumentSnapshot<Map<String, dynamic>>>(
        stream: oneUserDetails.snapshots(),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>> snapshot) {
          if (snapshot.data == null) return Center(child: ZText(content: ""));
          UserProfile userProfile = UserProfile.buildObjectAsync(snapshot);
          newRole = userProfile.role;
          return userDetails(userProfile);
        });
  }

  Widget userDetails(UserProfile userProfile) {
    selectedRoom = userProfile.assignedRoom ?? "noRoomAssigned";
    // ?? roomList.first;
    return Card(
      elevation: 8,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      color: Color(Styling.primaryBackgroundColor).withOpacity(0.7),
      child: Container(
        child: Column(
          children: [
            if (widget.userRole == Fields.admin &&
                widget.userData.id != widget.userId &&
                widget.userData.role != Fields.admin)
              roomDropdown(userProfile),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.diagonal * 1,
                  vertical: SizeConfig.diagonal * 1),
              child: ZText(
                content: '${I18n.of(context).deactivateUser}',
                textAlign: TextAlign.center,
                fontWeight: FontWeight.bold,
                color: Color(Styling.textColor),
              ),
            ),
            Container(
              margin: EdgeInsets.all(SizeConfig.diagonal * 1),
              width: double.infinity,
              height: 1,
              color: Color(Styling.primaryColor),
            ),
            Container(
                child: Padding(
              padding: EdgeInsets.all(SizeConfig.diagonal * 1),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ZText(
                    content: '${I18n.of(context).name} :',
                    textAlign: TextAlign.center,
                    color: Color(Styling.textColor),
                  ),
                  ZText(
                    content: '${userProfile.name}',
                    textAlign: TextAlign.center,
                    color: Color(Styling.textColor),
                  ),
                ],
              ),
            )),
            Container(
              child: Padding(
                padding: EdgeInsets.all(SizeConfig.diagonal * 1),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ZText(
                      content: '${I18n.of(context).phone} :',
                      textAlign: TextAlign.center,
                      color: Color(Styling.textColor),
                    ),
                    ZText(
                      content: '${userProfile.phoneNumber}',
                      textAlign: TextAlign.center,
                      color: Color(Styling.textColor),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: EdgeInsets.all(SizeConfig.diagonal * 1),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ZText(
                      content: '${I18n.of(context).role} :',
                      textAlign: TextAlign.center,
                      color: Color(Styling.textColor),
                    ),
                    Row(
                      children: [
                        ZText(
                          content: '${userProfile.role}',
                          textAlign: TextAlign.center,
                          color: Color(Styling.textColor),
                        ),
                        if (widget.userRole == Fields.admin &&
                            userProfile.id != widget.userId)
                          SizedBox(width: SizeConfig.diagonal * 1),
                        if (widget.userRole == Fields.admin &&
                            userProfile.id != widget.userId)
                          InkWell(
                            onTap: () => changeRole(userProfile.id!),
                            child: ZText(
                              content: I18n.of(context).change,
                              textAlign: TextAlign.center,
                              color: Color(Styling.accentColor),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            if (widget.userRole == Fields.admin &&
                userProfile.id != widget.userId &&
                userProfile.role != Fields.admin)
              ZElevatedButton(
                  child: Text(userProfile.isActive
                      ? "${I18n.of(context).desactive}"
                      : "${I18n.of(context).active}"),
                  onpressed: () => _actionPression(userProfile)),
          ],
        ),
      ),
    );
  }

  void changeRole(String id) async {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: AlertDialog(
          title: ZText(
            content: I18n.of(context).changeRole,
            textAlign: TextAlign.center,
            fontWeight: FontWeight.bold,
          ),
          content: StatefulBuilder(
            builder: (BuildContext context, StateSetter dialogsetState) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  DropdownButton<String>(
                    value: newRole,
                    icon: const Icon(Icons.arrow_drop_down),
                    elevation: 8,
                    style: TextStyle(color: Color(Styling.textColor)),
                    underline: Container(
                      height: 2,
                      color: Color(Styling.primaryColor),
                    ),
                    onChanged: (String? newValue) {
                      if (mounted)
                        dialogsetState(() {
                          newRole = newValue!;
                        });
                    },
                    items:
                        roleList.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                  SizedBox(height: SizeConfig.diagonal * 2),
                  ZElevatedButton(
                    onpressed: () => updateRole(id),
                    topPadding: 0.0,
                    bottomPadding: 0.0,
                    child: ZText(
                      content: "${I18n.of(context).updateRole}",
                      color: Color(Styling.primaryBackgroundColor),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  void updateRole(String id) async {
    EasyLoading.show(status: I18n.of(context).loading);
    bool isOnline = await hasConnection();
    if (!isOnline) {
      EasyLoading.dismiss();
      Navigator.of(context).pop();
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: ZText(content: I18n.of(context).noInternet)));
    } else {
      try {
        await widget.db.updateRole(id, newRole);
        EasyLoading.dismiss();
        Navigator.of(context).pop();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: ZText(content: I18n.of(context).operationSucceeded)));
      } catch (e) {
        EasyLoading.dismiss();
        Navigator.of(context).pop();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: ZText(content: e.toString()),
          ),
        );
      }
    }
  }

  Widget roomDropdown(UserProfile userProfile) {
    // log("roomList length: ${roomList.length}");
    log("selectedRoom: $selectedRoom");

    return Padding(
      padding: EdgeInsets.all(SizeConfig.diagonal * 2),
      child: Flex(
        direction: deviceSize(context) == DeviceSize.mobile
            ? Axis.vertical
            : Axis.horizontal,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.diagonal * 1,
                vertical: SizeConfig.diagonal * 1),
            child: ZText(
              content: I18n.of(context).assignRoom,
              textAlign: TextAlign.center,
              fontWeight: FontWeight.bold,
              color: Color(Styling.textColor),
            ),
          ),
          StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
              stream: roomsStream.snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> snapshot) {
                if (snapshot.data == null)
                  return Center(
                    child: ZText(content: ""),
                  );

                return StatefulBuilder(
                  builder: (BuildContext context, StateSetter dialogsetState) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        DropdownButton<String>(
                            value: selectedRoom,
                            icon: const Icon(Icons.arrow_drop_down),
                            elevation: 8,
                            style: TextStyle(color: Color(Styling.textColor)),
                            underline: Container(
                              height: 2,
                              color: Color(Styling.primaryColor),
                            ),
                            onChanged: (String? newValue) {
                              // if (mounted)
                              dialogsetState(() {
                                selectedRoom = newValue!;
                              });
                            },
                            items: [
                              DropdownMenuItem<String>(
                                value: "noRoomAssigned",
                                child: Text(selectedRoomName(
                                    "noRoomAssigned", context)),
                              ),
                              ...snapshot.data!.docs.map(
                                  (DocumentSnapshot<Map<String, dynamic>>
                                      document) {
                                Room newRoom = Room.buildObject(document);
                                return DropdownMenuItem<String>(
                                  value: newRoom.id,
                                  child: Text(
                                      selectedRoomName(newRoom.id, context)),
                                );
                              }).toList(),
                            ]),
                      ],
                    );
                  },
                );
              }),
          ZElevatedButton(
            onpressed: () => assignRoom(userProfile.id!),
            topPadding: 0.0,
            bottomPadding: 0.0,
            child: ZText(
              content: I18n.of(context).assign,
              color: Color(Styling.primaryBackgroundColor),
            ),
          ),
        ],
      ),
    );
  }

  void assignRoom(String selectedUserId) async {
    EasyLoading.show(status: I18n.of(context).loading);
    bool isOnline = await hasConnection();
    if (!isOnline) {
      EasyLoading.dismiss();
      Navigator.of(context).pop();
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: ZText(content: I18n.of(context).noInternet)));
    } else {
      try {
        await widget.db.assignRoom(selectedUserId, selectedRoom);
        EasyLoading.dismiss();
        Navigator.of(context).pop();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: ZText(content: I18n.of(context).operationSucceeded)));
      } catch (e) {
        EasyLoading.dismiss();
        Navigator.of(context).pop();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: ZText(content: e.toString()),
          ),
        );
      }
    }
  }
}
