import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flip_card/flip_card.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pinput/pinput.dart';
import 'package:sunstone/Components/ZElevatedButton.dart';
import 'package:sunstone/Components/ZTextField.dart';
import 'package:sunstone/Helpers/Utils.dart';
import 'package:sunstone/Models/Chat.dart';
import 'package:sunstone/Models/Conversation.dart';
import 'package:sunstone/Models/Fields.dart';
import 'package:sunstone/Models/UserProfile.dart';
import 'package:sunstone/Pages/DashboardPage.dart';
import 'package:sunstone/i18n.dart';
import 'package:sunstone/Services/Authentication.dart';
import 'package:sunstone/Services/Database.dart';
import 'package:sunstone/Services/Messaging.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../Components/ZText.dart';
import 'TermsPage.dart';

class LoginPage extends StatefulWidget {
  final Authentication auth;
  final Database db;
  final Messaging messaging;
  final User? user;

  LoginPage({
    required this.auth,
    required this.db,
    required this.messaging,
    this.user,
  });

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  int _pageState = 0;
  int _emailLoginPageState = 0;

  String _selectedAreaCode = '+257';

  double _xOffset1 = 0;
  double _xOffset2 = 0;
  double _xOffset3 = 0;

  double _xOffset4 = 0;
  double _xOffset5 = 0;

  //final _pinPutController = TextEditingController();
  //final _pinPutFocusNode = FocusNode();

  GlobalKey<FormState> _pinKey = GlobalKey<FormState>();
  GlobalKey<FormState> _phoneKey = GlobalKey<FormState>();
  GlobalKey<FormState> _nameKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  String? _verificationcode;

  String? _phoneNumber;
  User? _firebaseUser;

  String? _name;
  bool? _agreed = false;

  String _pinText = '';
  FlipCardController? _flipCardController = FlipCardController();

  GlobalKey<FormState> _pinKeyLarge = GlobalKey<FormState>();
  GlobalKey<FormState> _phoneKeyLarge = GlobalKey<FormState>();
  GlobalKey<FormState> _nameKeyLarge = GlobalKey<FormState>();

  GlobalKey<FormState> _emailFormKey = GlobalKey<FormState>();
  GlobalKey<FormState> _emailFormKey2 = GlobalKey<FormState>();

  GlobalKey<FormState> _emailFormKey3 = GlobalKey<FormState>();
  GlobalKey<FormState> _emailFormKey4 = GlobalKey<FormState>();

  String? _email;
  String? _password;
  String? _email1;
  String? _password1;
  String? _email3;
  String? _password3;
  String? _email4;
  String? _password4;
  String? _confirmPassword;

  String? _name1;
  String? _phoneNumber1;
  String? _name2;
  String? _phoneNumber2;

  /* @override
  void initState() {
    super.initState();
    unawaited(widget.auth.signOut());
  }*/

  // FlipCardController flipCardController = FlipCardController();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final node = FocusScope.of(context);

    switch (_pageState) {
      case 0:
        if (mounted)
          setState(() {
            _xOffset1 = SizeConfig.safeBlockHorizontal * 0;
            _xOffset2 = SizeConfig.safeBlockHorizontal * 200;
            _xOffset3 = SizeConfig.safeBlockHorizontal * 200;
          });
        break;
      case 1:
        if (mounted)
          setState(() {
            _xOffset1 = SizeConfig.safeBlockHorizontal * -200;
            _xOffset2 = SizeConfig.safeBlockHorizontal * 0;
            _xOffset3 = SizeConfig.safeBlockHorizontal * 200;
          });
        break;
      case 2:
        if (mounted)
          setState(() {
            _xOffset1 = SizeConfig.safeBlockHorizontal * -200;
            _xOffset2 = SizeConfig.safeBlockHorizontal * -200;
            _xOffset3 = SizeConfig.safeBlockHorizontal * 0;
          });
    }

    switch (_emailLoginPageState) {
      case 0:
        if (mounted)
          setState(() {
            _xOffset4 = SizeConfig.safeBlockHorizontal * 0;
            _xOffset5 = SizeConfig.safeBlockHorizontal * 200;
          });
        break;
      case 1:
        if (mounted)
          setState(() {
            _xOffset4 = SizeConfig.safeBlockHorizontal * -200;
            _xOffset5 = SizeConfig.safeBlockHorizontal * 0;
          });
        break;
      default:
    }

    /*return Scaffold(
      key: _scaffoldKey,
      body: Center(
        child: SingleChildScrollView(
          child: Stack(
            children: [Text('hello')],
          ),
        ),
      ),
    );*/

    return Scaffold(
      key: _scaffoldKey,
      body: deviceSize(context) != DeviceSize.mobile
          ? loginCard(context, node)
          : SafeArea(
              child: FlipCard(
                flipOnTouch: false,
                back: Container(
                  height: double.infinity,
                  margin: EdgeInsets.all(SizeConfig.diagonal * 1),
                  child: Card(
                    color:
                        Color(Styling.primaryBackgroundColor).withOpacity(0.7),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: SingleChildScrollView(
                      controller: ScrollController(),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                              SizeConfig.diagonal * 1,
                              SizeConfig.diagonal * 1,
                              SizeConfig.diagonal * 1,
                              SizeConfig.diagonal * 5,
                            ),
                            child: Row(
                              children: [
                                IconButton(
                                  onPressed: () {
                                    setState(() {
                                      _flipCardController!.toggleCard();
                                    });
                                  },
                                  icon: Icon(
                                    Icons.arrow_back,
                                  ),
                                ),
                                Expanded(child: Container()),
                              ],
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                width: SizeConfig.diagonal * 10,
                                height: SizeConfig.diagonal * 10,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                  image: AssetImage('assets/logo.png'),
                                  fit: BoxFit.cover,
                                )),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.all(SizeConfig.diagonal * 1),
                                child: ClipRect(
                                  child: SingleChildScrollView(
                                    child: Stack(
                                      children: <Widget>[
                                        existingUserEmailSignInMobile(context),
                                        registerWithEmailMobile(context),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                controller: _flipCardController,
                front: Container(
                  margin: EdgeInsets.all(SizeConfig.diagonal * 1),
                  height: double.infinity,
                  child: Card(
                    color:
                        Color(Styling.primaryBackgroundColor).withOpacity(0.7),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          width: SizeConfig.diagonal * 10,
                          height: SizeConfig.diagonal * 10,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                            image: AssetImage('assets/logo.png'),
                            fit: BoxFit.cover,
                          )),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(bottom: SizeConfig.diagonal * 6),
                          child: ZText(
                            content:
                                I18n.of(context).createaccount.toUpperCase(),
                            color: Color(Styling.accentColor),
                            fontSize: 20,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                        SingleChildScrollView(
                          child: Stack(
                            children: <Widget>[
                              AnimatedContainer(
                                curve: Curves.easeInBack,
                                duration: Duration(milliseconds: 1000),
                                transform:
                                    Matrix4.translationValues(_xOffset1, 0, 1),
                                child: Padding(
                                  padding:
                                      EdgeInsets.all(SizeConfig.diagonal * 3),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Form(
                                        key: _phoneKey,
                                        child: ZTextField(
                                          outsidePrefix:
                                              ZText(content: _selectedAreaCode),
                                          hint:
                                              I18n.of(context).yourphonenumber,
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            FilteringTextInputFormatter
                                                .digitsOnly
                                          ],
                                          onFieldSubmitted: (String value) =>
                                              sendCode(),
                                          onSaved: (newValue) =>
                                              _phoneNumber = newValue,
                                          validator: (value) => value == null ||
                                                  value.isEmpty
                                              ? I18n.of(context).requiredInput
                                              : null,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      ZElevatedButton(
                                        onpressed: sendCode,
                                        child: ZText(
                                          content: I18n.of(context).signin,
                                          color: Color(
                                              Styling.primaryBackgroundColor),
                                        ),
                                      ),
                                      loginWithEmailText(context),
                                    ],
                                  ),
                                ),
                              ),
                              AnimatedContainer(
                                curve: Curves.easeInBack,
                                duration: Duration(milliseconds: 1000),
                                transform:
                                    Matrix4.translationValues(_xOffset2, 0, 1),
                                child: Padding(
                                  padding:
                                      EdgeInsets.all(SizeConfig.diagonal * 1),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Form(
                                        key: _pinKey,
                                        child: Pinput(
                                          length: 6,
                                          onCompleted: (pin) => _pinText = pin,
                                          defaultPinTheme: PinTheme(
                                            width: 56,
                                            height: 56,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Color(
                                                      Styling.accentColor)),
                                              //borderRadius: BorderRadius.circular(20),
                                            ),
                                          ),
                                        ),
                                        /*child: Pinput(
                                    eachFieldWidth: SizeConfig.diagonal * 1,
                                    eachFieldHeight: SizeConfig.diagonal * 1,
                                    textInputAction: TextInputAction.go,
                                    /*onSubmit: (String value) =>
                                          confirmSMSCode(value),*/
                                    withCursor: false,
                                    fieldsCount: 6,
                                    focusNode: _pinPutFocusNode,
                                    controller: _pinPutController,
                                    submittedFieldDecoration: BoxDecoration(
                                      border: Border.all(),
                                      color: Color(Styling.iconColor),
                                      borderRadius: BorderRadius.circular(6),
                                    ),
                                    selectedFieldDecoration: BoxDecoration(
                                      border: Border.all(),
                                      color: Color(Styling.primaryBackgroundColor),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    followingFieldDecoration: BoxDecoration(
                                      border: Border.all(),
                                      color: Color(Styling.primaryBackgroundColor),
                                      borderRadius: BorderRadius.circular(6),
                                    ),
                                    pinAnimationType: PinAnimationType.scale,
                                    textStyle: TextStyle(
                                        color: Color(Styling.primaryDarkColor),
                                        fontSize: SizeConfig.diagonal * 3),
                                  ),*/
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      ZElevatedButton(
                                        child: ZText(
                                            content: I18n.of(context).confirm,
                                            color: Color(Styling
                                                .primaryBackgroundColor)),
                                        onpressed: () =>
                                            confirmSMSCode(_pinText),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              AnimatedContainer(
                                curve: Curves.easeInBack,
                                duration: Duration(milliseconds: 1000),
                                transform:
                                    Matrix4.translationValues(_xOffset3, 0, 1),
                                child: Center(
                                  child: Padding(
                                    padding:
                                        EdgeInsets.all(SizeConfig.diagonal * 1),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Form(
                                          key: _nameKey,
                                          child: Column(
                                            children: [
                                              ZTextField(
                                                hint: I18n.of(context).yourname,
                                                onEditingComplete: () =>
                                                    node.nextFocus(),
                                                onSaved: (newValue) =>
                                                    _name = newValue,
                                                keyboardType:
                                                    TextInputType.text,
                                              ),
                                              SizedBox(
                                                height:
                                                    SizeConfig.diagonal * 10,
                                              ),
                                              /* Row(
                                          children: [
                                            Theme(
                                              data: ThemeData(
                                                  unselectedWidgetColor:
                                                      Colors.black),
                                              child: Checkbox(
                                                value: _agreed,
                                                onChanged: (bool? newValue) {
                                                  setState(() {
                                                    _agreed = newValue;
                                                  });
                                                },
                                                activeColor:
                                                    Color(Styling.accentColor),
                                                checkColor: Color(
                                                    Styling.primaryBackgroundColor),
                                              ),
                                            ),
                                            TextButton(
                                              onPressed: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) => TermsPage(
                                                      auth: widget.auth,
                                                      db: widget.db,
                                                    ),
                                                  ),
                                                );
                                              },
                                              child: RichText(
                                                text: TextSpan(
                                                    text:
                                                        I18n.of(context).readAndAgree,
                                                    style: TextStyle(
                                                      color: Color(Styling.textColor),
                                                     
                                                    ),
                                                    children: [
                                                      TextSpan(
                                                        text: I18n.of(context).terms,
                                                        style: TextStyle(
                                                          color: Color(
                                                              Styling.accentColor),
                                                        
                                                          decoration: TextDecoration
                                                              .underline,
                                                        ),
                                                      ),
                                                    ]),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: SizeConfig.diagonal * 1,
                                        ),*/
                                            ],
                                          ),
                                        ),
                                        ZElevatedButton(
                                          onpressed: createAccount,
                                          child: ZText(
                                            content: I18n.of(context).signUp,
                                            color: Color(
                                                Styling.primaryBackgroundColor),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  void confirmSMSCode(String value) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (value.length == 6) {
      EasyLoading.show(status: I18n.of(context).loading);
      try {
        PhoneAuthCredential credential = PhoneAuthProvider.credential(
            verificationId: _verificationcode!, smsCode: value);
        UserCredential user =
            await widget.auth.getAuth().signInWithCredential(credential);
        isProfileCreated(user);
      } on Exception catch (e) {
        EasyLoading.dismiss();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: ZText(content: I18n.of(context).operationFailed)));
      }
    }
  }

  void isProfileCreated(UserCredential userCredential) async {
    final User user = userCredential.user!;
    /*String token = await widget.messaging.firebaseMessaging.getToken();
    await widget.db.setToken(user.uid, token);*/
    UserProfile? userProfile = await widget.db.getUserProfile(user.uid);
    if (userProfile == null) {
      EasyLoading.dismiss();
      if (mounted)
        setState(() {
          _pageState = 2;
          _firebaseUser = user;
        });
    } else {
      String? token = await widget.messaging.firebaseMessaging
          .getToken(
              vapidKey:
                  'BH5GtQOTwQbW8DjT2F2DdqhxyKLnK5NaEBYXtz658XCHbNO6pTAIPfTp9E_UPRRLSAhmrE0g_k098OSySSAwPdA')
          .timeout(Duration(seconds: 15), onTimeout: () {
        return null;
      });
      await widget.db.setToken(user.uid, token);
      await widget.db.getConfiguration().timeout(Duration(seconds: 180),
          onTimeout: () {
        widget.db.useImages = true;
        widget.db.callNumber = "69751595";
      });
      Chat? chat;
      if (userProfile.role == Fields.client) {
        chat = await widget.db.getChat(user.uid);
      }

      EasyLoading.dismiss();
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => DashboardPage(
            auth: widget.auth,
            userId: user.uid,
            userRole: userProfile.role,
            db: widget.db,
            messaging: widget.messaging,
            userName: userProfile.name,
            chat: chat,
            userProfile: userProfile,
          ),
        ),
      );
    }
  }

  bool validate() {
    FormState? form;
    if (deviceSize(context) != DeviceSize.mobile) {
      form = _phoneKeyLarge.currentState;
    } else {
      form = _phoneKey.currentState;
    }

    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void createAccount() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (validate2()) {
      if (_agreed == true) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: ZText(content: I18n.of(context).acceptConditions)));
      } else {
        EasyLoading.show(status: I18n.of(context).loading);
        bool isOnline = await hasConnection();
        if (!isOnline) {
          EasyLoading.dismiss();
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: ZText(content: I18n.of(context).noInternet)));
        } else {
          try {
            User currentUser;
            if (_firebaseUser != null)
              currentUser = _firebaseUser!;
            else
              currentUser = widget.user!;

            String? token = await widget.messaging.firebaseMessaging
                .getToken()
                .timeout(Duration(seconds: 15), onTimeout: () {
              return null;
            });
            UserProfile userProfile = UserProfile(
              id: currentUser.uid,
              name: _name!,
              role: Fields.client,
              phoneNumber: currentUser.phoneNumber!,
              token: token,
              isActive: false,
              assignedRoom: "noRoomAssigned",
              tags: getUserTags(_name!, currentUser.phoneNumber!),
            );
            await widget.db.createAccount(context, userProfile);
            await widget.db.getConfiguration().timeout(Duration(seconds: 180),
                onTimeout: () {
              widget.db.useImages = true;
              widget.db.callNumber = "69751595";
            });

            Chat? chat = await createChat(userProfile);

            EasyLoading.dismiss();

            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => DashboardPage(
                  auth: widget.auth,
                  userId: userProfile.id!,
                  userRole: userProfile.role,
                  db: widget.db,
                  messaging: widget.messaging,
                  userName: userProfile.name,
                  chat: chat,
                  userProfile: userProfile,
                ),
              ),
            );
          } on Exception catch (e) {
            EasyLoading.dismiss();
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: ZText(content: I18n.of(context).operationFailed)));
          }
        }
      }
    }
  }

  Future<Chat?> createChat(UserProfile userProfile) async {
    Chat? chat;
    if (userProfile.role == Fields.client) {
      DocumentReference newChatReference =
          widget.db.databaseReference.collection(Fields.chats).doc();
      chat = Chat(
        customerName: userProfile.name,
        customerId: userProfile.id!,
        customerUnreadCount: 0,
        adminUnreadCount: 0,
        id: newChatReference.id,
      );

      await widget.db.addChat(
          userProfile.id!, newChatReference, chat, newChatReference.id);

      DocumentReference conversationDocument = widget.db.databaseReference
          .collection(Fields.chats)
          .doc(newChatReference.id)
          .collection(Fields.conversations)
          .doc();

      Conversation conversation = Conversation(
        id: conversationDocument.id,
        message: I18n.of(context).welcomeMessage,
        writtenById: Fields.admin,
        chatId: newChatReference.id,
        writtenToId: userProfile.id!,
        writtenAt: null,
        localTime: DateTime.now().millisecondsSinceEpoch,
        type: Fields.text,
      );

      conversation = await widget.db.addMessage(
          chat, newChatReference.id, conversation, false, conversationDocument);
    }
    return chat;
  }

  bool validate2() {
    final form = _nameKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void sendCode() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (validate()) {
      EasyLoading.show(status: I18n.of(context).loading);
      bool isOnline = await hasConnection();
      if (!isOnline) {
        EasyLoading.dismiss();
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: ZText(content: I18n.of(context).noInternet)));
      } else {
        try {
          String number = _selectedAreaCode + _phoneNumber!;
          if (kIsWeb) {
            ConfirmationResult result =
                await FirebaseAuth.instance.signInWithPhoneNumber(number);
            if (mounted)
              setState(() {
                _verificationcode = result.verificationId;
              });
            EasyLoading.dismiss();
            if (mounted)
              setState(() {
                _pageState = 1;
              });
          } else {
            await FirebaseAuth.instance.verifyPhoneNumber(
              phoneNumber: number,
              verificationCompleted: (PhoneAuthCredential credential) async {
                UserCredential user = await widget.auth
                    .getAuth()
                    .signInWithCredential(credential);
                isProfileCreated(user);
              },
              verificationFailed: (FirebaseAuthException e) {
                EasyLoading.dismiss();
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: ZText(content: e.message!),
                  ),
                );
              },
              codeSent: (String verificationId, int? resendToken) {
                if (mounted)
                  setState(() {
                    _verificationcode = verificationId;
                  });

                bool isAndroid = Platform.isAndroid;
                if (!isAndroid) {
                  EasyLoading.dismiss();
                  if (mounted)
                    setState(() {
                      _pageState = 1;
                    });
                }
              },
              timeout: Duration(seconds: 5),
              codeAutoRetrievalTimeout: (String verificationId) {
                if (mounted)
                  setState(() {
                    _verificationcode = verificationId;
                  });
                bool isAndroid = Platform.isAndroid;
                if (isAndroid) {
                  EasyLoading.dismiss();
                  if (mounted)
                    setState(() {
                      _pageState = 1;
                    });
                }
              },
            );
          }
        } on Exception catch (e) {
          EasyLoading.dismiss();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: ZText(content: I18n.of(context).operationFailed)));
        }
      }
    }
  }

  Widget loginCard(BuildContext context, FocusNode node) {
    return Center(
      child: SizedBox(
        // height: SizeConfig.diagonal * 60,
        width: SizeConfig.diagonal * 40,
        child: FlipCard(
          flipOnTouch: false,
          controller: _flipCardController,
          front: SingleChildScrollView(
            controller: ScrollController(),
            child: Card(
              color: Color(Styling.primaryBackgroundColor).withOpacity(0.7),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig.diagonal * 1),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      width: SizeConfig.diagonal * 10,
                      height: SizeConfig.diagonal * 10,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                        image: AssetImage('assets/logo.png'),
                        fit: BoxFit.cover,
                      )),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    ZText(
                      content: I18n.of(context).createprofile.toUpperCase(),
                      color: Color(Styling.accentColor),
                      fontSize: 20,
                      fontStyle: FontStyle.normal,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    ClipRect(
                      child: SingleChildScrollView(
                        child: Stack(
                          alignment: AlignmentDirectional.center,
                          children: [
                            numberInputLargeScreen(),
                            otpInputLargeScreen(),
                            nameInputLargeScreen(context, node),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    loginWithEmailText(context),
                    SizedBox(
                      height: 60,
                    ),
                  ],
                ),
              ),
            ),
          ),
          back: SingleChildScrollView(
            controller: ScrollController(),
            child: loginWithEmailCard(context),
          ),
        ),
      ),
    );
  }

  Widget numberInputLargeScreen() {
    return AnimatedContainer(
      curve: Curves.easeInBack,
      duration: Duration(milliseconds: 1000),
      transform: Matrix4.translationValues(_xOffset1, 0, 1),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Form(
            key: _phoneKeyLarge,
            child: ZTextField(
              outsidePrefix: ZText(content: _selectedAreaCode),
              hint: I18n.of(context).yourphonenumber,
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              onFieldSubmitted: (String value) => sendCode(),
              onSaved: (newValue) => _phoneNumber = newValue,
              validator: (value) => value == null || value.isEmpty
                  ? I18n.of(context).requiredInput
                  : null,
            ),
          ),
          SizedBox(
            height: 30,
          ),
          ZElevatedButton(
            onpressed: sendCode,
            child: ZText(
              content: I18n.of(context).signin,
              color: Color(Styling.primaryBackgroundColor),
            ),
          ),
        ],
      ),
    );
  }

  Widget otpInputLargeScreen() {
    return AnimatedContainer(
      curve: Curves.easeInBack,
      duration: Duration(milliseconds: 1000),
      transform: Matrix4.translationValues(_xOffset2, 0, 1),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Form(
            key: _pinKeyLarge,
            child: Pinput(
              length: 6,
              onCompleted: (pin) => _pinText = pin,
              defaultPinTheme: PinTheme(
                width: 56,
                height: 56,
                decoration: BoxDecoration(
                  border: Border.all(color: Color(Styling.accentColor)),
                  //borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          ZElevatedButton(
            child: ZText(
                content: I18n.of(context).confirm,
                color: Color(Styling.primaryBackgroundColor)),
            onpressed: () => confirmSMSCode(_pinText),
          ),
        ],
      ),
    );
  }

  Widget nameInputLargeScreen(BuildContext context, FocusNode node) {
    return AnimatedContainer(
      curve: Curves.easeInBack,
      duration: Duration(milliseconds: 1000),
      transform: Matrix4.translationValues(_xOffset3, 0, 1),
      child: Column(
        children: [
          Form(
            key: _nameKeyLarge,
            child: Column(
              children: [
                ZTextField(
                  hint: I18n.of(context).yourname,
                  onEditingComplete: () => node.nextFocus(),
                  onSaved: (newValue) => _name = newValue,
                  keyboardType: TextInputType.text,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
          ZElevatedButton(
            onpressed: createAccount,
            child: ZText(
              content: I18n.of(context).signUp,
              color: Color(Styling.primaryBackgroundColor),
            ),
          ),
        ],
      ),
    );
  }

  Widget loginWithEmailText(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ZText(
          content: "or ",
          color: Color(Styling.textColor),
          fontSize: 10,
        ),
        SizedBox(
          width: 10,
        ),
        InkWell(
          onTap: () {
            _flipCardController!.toggleCard();
          },
          child: ZText(
            content: I18n.of(context).loginWithEmail,
            color: Color(Styling.textColor),
          ),
        ),
      ],
    );
  }

  Widget loginWithEmailCard(BuildContext context) {
    return Card(
      color: Color(Styling.primaryBackgroundColor).withOpacity(0.7),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: EdgeInsets.all(SizeConfig.diagonal * 1),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    _flipCardController!.toggleCard();
                  },
                  icon: Icon(
                    Icons.arrow_back,
                  ),
                ),
                Expanded(child: Container()),
              ],
            ),
            Container(
              width: SizeConfig.diagonal * 10,
              height: SizeConfig.diagonal * 10,
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: AssetImage('assets/logo.png'),
                fit: BoxFit.cover,
              )),
            ),
            ClipRect(
                child: SingleChildScrollView(
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  existingUserEmailSignIn(context),
                  registerWithEmail(context),
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }

  Widget existingUserEmailSignIn(BuildContext context) {
    return AnimatedContainer(
      curve: Curves.easeInBack,
      duration: Duration(milliseconds: 1000),
      transform: Matrix4.translationValues(_xOffset4, 0, 1),
      child: Form(
        key: _emailFormKey,
        child: Column(
          children: [
            SizedBox(
              height: 40,
            ),
            ZText(
              content: I18n.of(context).loginWithEmail.toUpperCase(),
              color: Color(Styling.accentColor),
              fontSize: 20,
              fontStyle: FontStyle.normal,
            ),
            SizedBox(
              height: 30,
            ),
            ZTextField(
              outsidePrefix: Icon(
                Icons.alternate_email,
                size: 16,
              ),
              hint: I18n.of(context).emailAdress,
              onSaved: (newValue) {
                setState(() {
                  _email = newValue;
                });
              },
              validator: (value) => emailValidator(context, value),
            ),
            SizedBox(
              height: 15,
            ),
            ZTextField(
              outsidePrefix: Icon(
                Icons.password,
                size: 16,
              ),
              hint: I18n.of(context).password,
              onSaved: (newValue) {
                setState(() {
                  _password = newValue;
                });
              },
              validator: (value) => passwordValidator(context, value),
              obsecure: true,
            ),
            SizedBox(
              height: 30,
            ),
            ZElevatedButton(
              onpressed: () => signInWithEmail(context),
              child: ZText(
                content: I18n.of(context).signin,
                color: Color(Styling.primaryBackgroundColor),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () {
                setState(() {
                  _emailLoginPageState = 1;
                });
              },
              child: ZText(
                content: I18n.of(context).firstLoginWithEmail,
                fontSize: 14,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget existingUserEmailSignInMobile(BuildContext context) {
    return AnimatedContainer(
        curve: Curves.easeInBack,
        duration: Duration(milliseconds: 1000),
        transform: Matrix4.translationValues(_xOffset4, 0, 1),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: SizeConfig.diagonal * 2),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ZText(
                content: I18n.of(context).loginWithEmail.toUpperCase(),
                color: Color(Styling.accentColor),
                fontSize: 20,
                fontStyle: FontStyle.normal,
              ),
              Form(
                key: _emailFormKey3,
                child: Column(
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    ZTextField(
                      outsidePrefix: Icon(
                        Icons.alternate_email,
                        size: 16,
                      ),
                      hint: I18n.of(context).emailAdress,
                      onSaved: (newValue) {
                        setState(() {
                          _email3 = newValue;
                        });
                      },
                      validator: (value) => emailValidator(context, value),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    ZTextField(
                      outsidePrefix: Icon(
                        Icons.password,
                        size: 16,
                      ),
                      hint: I18n.of(context).password,
                      onSaved: (newValue) {
                        setState(() {
                          _password3 = newValue;
                        });
                      },
                      validator: (value) => passwordValidator(context, value),
                      obsecure: true,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    ZElevatedButton(
                      onpressed: () => signInWithEmail(context),
                      child: ZText(
                        content: I18n.of(context).signin,
                        color: Color(Styling.primaryBackgroundColor),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          _emailLoginPageState = 1;
                        });
                      },
                      child: ZText(
                        content: I18n.of(context).firstLoginWithEmail,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  Widget registerWithEmail(BuildContext context) {
    return AnimatedContainer(
      curve: Curves.easeInBack,
      duration: Duration(milliseconds: 1000),
      transform: Matrix4.translationValues(_xOffset5, 0, 1),
      child: Row(
        children: [
          Container(
            // height: SizeConfig.diagonal * 15,
            // width: 10,
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_new_sharp),
              onPressed: () {
                setState(() {
                  _emailLoginPageState = 0;
                });
              },
            ),
          ),
          Container(
            child: Form(
              key: _emailFormKey2,
              child: Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 5,
                    ),
                    ZText(
                      content: I18n.of(context).register.toUpperCase(),
                      color: Color(Styling.accentColor),
                      fontSize: 20,
                      fontStyle: FontStyle.normal,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    ZTextField(
                      outsidePrefix: Icon(
                        Icons.person,
                        size: 16,
                      ),
                      onSaved: (newValue) {
                        _name1 = newValue;
                      },
                      hint: I18n.of(context).yourname,
                      keyboardType: TextInputType.name,
                      validator: (String? value) =>
                          (value == null || value.trim().isEmpty)
                              ? I18n.of(context).requiredInput
                              : null,
                      inputFormatters: [
                        FilteringTextInputFormatter.singleLineFormatter
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    ZTextField(
                      outsidePrefix: Icon(
                        Icons.phone,
                        size: 16,
                      ),
                      hint: I18n.of(context).yourphonenumber,
                      onSaved: (value) {
                        _phoneNumber1 = value;
                      },
                      keyboardType: TextInputType.number,
                      validator: (String? value) =>
                          (value == null || value.trim().isEmpty)
                              ? I18n.of(context).requiredInput
                              : null,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    ZTextField(
                      outsidePrefix: Icon(
                        Icons.alternate_email,
                        size: 16,
                      ),
                      onSaved: (newValue) {
                        _email1 = newValue;
                      },
                      hint: I18n.of(context).emailAdress,
                      validator: (value) => emailValidator(context, value),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    ZTextField(
                      outsidePrefix: Icon(
                        Icons.password,
                        size: 16,
                      ),
                      hint: I18n.of(context).password,
                      onSaved: (newValue) {
                        _password1 = newValue;
                      },
                      onChanged: (value) => _password1 = value,
                      validator: (value) => passwordValidator(context, value),
                      obsecure: true,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    ZTextField(
                      outsidePrefix: Icon(
                        Icons.password,
                        size: 16,
                      ),
                      hint: I18n.of(context).confirmPassword,
                      onSaved: (value) {
                        _confirmPassword = value;
                      },
                      validator: (newValue) =>
                          cPasswordValidator(context, newValue, _password1),
                      obsecure: true,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    ZElevatedButton(
                      onpressed: () => registerEmail(context),
                      child: ZText(
                        content: I18n.of(context).createaccount,
                        color: Color(Styling.primaryBackgroundColor),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget registerWithEmailMobile(BuildContext context) {
    return AnimatedContainer(
        curve: Curves.easeInBack,
        duration: Duration(milliseconds: 1000),
        transform: Matrix4.translationValues(_xOffset5, 0, 1),
        child: Row(
          children: [
            Container(
              height: SizeConfig.diagonal * 15,
              // width: 10,
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_new_sharp),
                onPressed: () {
                  setState(() {
                    _emailLoginPageState = 0;
                  });
                },
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(
                child: Padding(
                  padding: EdgeInsets.only(
                      right: SizeConfig.diagonal * 2, left: 0.0),
                  child: Column(
                    children: <Widget>[
                      ZText(
                        content: I18n.of(context).register.toUpperCase(),
                        color: Color(Styling.accentColor),
                        fontSize: 20,
                        fontStyle: FontStyle.normal,
                      ),
                      Form(
                        key: _emailFormKey4,
                        child: Column(
                          children: [
                            SizedBox(
                              height: 8,
                            ),
                            ZTextField(
                              outsidePrefix: Icon(
                                Icons.person,
                                size: 16,
                              ),
                              onSaved: (newValue) {
                                _name2 = newValue;
                              },
                              hint: I18n.of(context).yourname,
                              keyboardType: TextInputType.name,
                              validator: (String? value) =>
                                  (value == null || value.trim().isEmpty)
                                      ? I18n.of(context).requiredInput
                                      : null,
                              inputFormatters: [
                                FilteringTextInputFormatter.singleLineFormatter
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            ZTextField(
                              outsidePrefix: Icon(
                                Icons.phone,
                                size: 16,
                              ),
                              hint: I18n.of(context).yourphonenumber,
                              onSaved: (value) {
                                _phoneNumber2 = value;
                              },
                              validator: (String? value) =>
                                  (value == null || value.trim().isEmpty)
                                      ? I18n.of(context).requiredInput
                                      : null,
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            ZTextField(
                              outsidePrefix: Icon(
                                Icons.alternate_email,
                                size: 16,
                              ),
                              onSaved: (newValue) {
                                _email4 = newValue;
                              },
                              hint: I18n.of(context).emailAdress,
                              validator: (value) =>
                                  emailValidator(context, value),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            ZTextField(
                              outsidePrefix: Icon(
                                Icons.password,
                                size: 16,
                              ),
                              hint: I18n.of(context).password,
                              onSaved: (newValue) {
                                _password1 = newValue;
                              },
                              onChanged: (value) => _password4 = value,
                              validator: (value) =>
                                  passwordValidator(context, value),
                              obsecure: true,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            ZTextField(
                              outsidePrefix: Icon(
                                Icons.password,
                                size: 16,
                              ),
                              hint: I18n.of(context).confirmPassword,
                              onSaved: (value) {
                                _confirmPassword = value;
                              },
                              validator: (newValue) => cPasswordValidator(
                                  context, newValue, _password4),
                              obsecure: true,
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            ZElevatedButton(
                              onpressed: () => registerEmail(context),
                              child: ZText(
                                content: I18n.of(context).createaccount,
                                color: Color(Styling.primaryBackgroundColor),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  bool validateEmail() {
    final form = _emailFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  bool validateEmail2() {
    final form = _emailFormKey2.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  bool validateEmail3() {
    final form = _emailFormKey3.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  bool validateEmail4() {
    final form = _emailFormKey4.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void signInWithEmail(BuildContext context) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    log("email value ${_email}");
    bool _validateForm = deviceSize(context) == DeviceSize.mobile
        ? validateEmail3()
        : validateEmail();
    if (_validateForm) {
      log("validated");
      EasyLoading.show(status: I18n.of(context).loading);
      bool isOnline = await hasConnection();
      if (!isOnline) {
        EasyLoading.dismiss();
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: ZText(content: I18n.of(context).noInternet)));
      } else {
        try {
          User? currentUser = await widget.auth.signInWithEmail(
              deviceSize(context) == DeviceSize.mobile ? _email3! : _email!,
              deviceSize(context) == DeviceSize.mobile
                  ? _password3!
                  : _password!);

          if (currentUser == null) {
            EasyLoading.dismiss();
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: ZText(content: I18n.of(context).wrongPasswordEmail)));
          } else {
            UserProfile? userProfile =
                await widget.db.getUserProfile(currentUser.uid);

            String? token = await widget.messaging.firebaseMessaging
                .getToken(
                    vapidKey:
                        'BH5GtQOTwQbW8DjT2F2DdqhxyKLnK5NaEBYXtz658XCHbNO6pTAIPfTp9E_UPRRLSAhmrE0g_k098OSySSAwPdA')
                .timeout(Duration(seconds: 15), onTimeout: () {
              return null;
            });
            await widget.db.setToken(currentUser.uid, token);
            await widget.db.getConfiguration().timeout(Duration(seconds: 180),
                onTimeout: () {
              widget.db.useImages = true;
              widget.db.callNumber = "69751595";
            });
            Chat? chat;
            if (userProfile!.role == Fields.client) {
              chat = await widget.db.getChat(currentUser.uid);
            }

            EasyLoading.dismiss();
            if (userProfile.id != null) {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => DashboardPage(
                    auth: widget.auth,
                    userId: currentUser.uid,
                    userRole: userProfile.role,
                    userName: userProfile.name,
                    db: widget.db,
                    messaging: widget.messaging,
                    chat: chat,
                    userProfile: userProfile,
                  ),
                ),
              );
            }
          }
        } catch (e) {
          log("error found");
          EasyLoading.dismiss();
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: ZText(content: "$e")));
        }
      }
    }
  }

  void registerEmail(BuildContext context) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    bool _validateForm = deviceSize(context) == DeviceSize.mobile
        ? validateEmail4()
        : validateEmail2();
    if (_validateForm) {
      EasyLoading.show(status: I18n.of(context).loading);
      bool isOnline = await hasConnection();
      if (!isOnline) {
        EasyLoading.dismiss();
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: ZText(content: I18n.of(context).noInternet)));
      } else {
        try {
          User? currentUser = await widget.auth.registerWithEmail(
              deviceSize(context) == DeviceSize.mobile ? _email4! : _email1!,
              deviceSize(context) == DeviceSize.mobile
                  ? _password4!
                  : _password1!);

          if (currentUser == null) {
            EasyLoading.dismiss();
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: ZText(content: I18n.of(context).operationFailed)));
          } else {
            String? token = await widget.messaging.firebaseMessaging
                .getToken(
                    vapidKey:
                        'BH5GtQOTwQbW8DjT2F2DdqhxyKLnK5NaEBYXtz658XCHbNO6pTAIPfTp9E_UPRRLSAhmrE0g_k098OSySSAwPdA')
                .timeout(Duration(seconds: 15), onTimeout: () {
              return null;
            });

            UserProfile userProfile = UserProfile(
              id: currentUser.uid,
              name:
                  deviceSize(context) == DeviceSize.mobile ? _name2! : _name1!,
              role: Fields.client,
              phoneNumber: deviceSize(context) == DeviceSize.mobile
                  ? _phoneNumber2!
                  : _phoneNumber1!,
              token: token,
              isActive: false,
              tags: getUserTags(
                  deviceSize(context) == DeviceSize.mobile ? _name2! : _name1!,
                  deviceSize(context) == DeviceSize.mobile
                      ? _phoneNumber2!
                      : _phoneNumber1!),
              assignedRoom: "noRoomAssigned",
            );
            await widget.db.createAccount(context, userProfile);
            await widget.db.getConfiguration().timeout(Duration(seconds: 180),
                onTimeout: () {
              widget.db.useImages = true;
              widget.db.callNumber = "69751595";
            });

            Chat? chat = await createChat(userProfile);

            EasyLoading.dismiss();

            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => DashboardPage(
                  auth: widget.auth,
                  userId: currentUser.uid,
                  userRole: userProfile.role,
                  db: widget.db,
                  messaging: widget.messaging,
                  userName: userProfile.name,
                  chat: chat,
                  userProfile: userProfile,
                ),
              ),
            );
          }
        } catch (e) {
          EasyLoading.dismiss();
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: ZText(content: "$e")));
        }
      }
    }
  }
}
