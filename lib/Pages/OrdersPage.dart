import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Helpers/Utils.dart';
import 'package:sunstone/Models/Chat.dart';
import 'package:sunstone/Models/Fields.dart';
import 'package:sunstone/Models/Order.dart';
import 'package:sunstone/Models/UserProfile.dart';
import 'package:sunstone/Pages/SingleOrderPage.dart';
import 'package:sunstone/Pages/UserPage.dart';
import 'package:sunstone/Services/Authentication.dart';
import 'package:intl/intl.dart';
import 'package:sunstone/Services/Database.dart';
import 'package:sunstone/Services/Messaging.dart';
import 'package:collection/collection.dart';
import 'package:responsive_scaffold/responsive_scaffold.dart';

import '../Components/ZText.dart';
import '../i18n.dart';

class OrdersPage extends StatefulWidget {
  final Authentication auth;
  final Database db;
  final String userId;
  final String userRole;
  final String userName;
  final Messaging messaging;
  final Chat? chat;
  final DateFormat formatter = DateFormat('dd/MM/yy HH:mm');
  final UserProfile userProfile;

  OrdersPage({
    required this.auth,
    required this.db,
    required this.userId,
    required this.userName,
    required this.userRole,
    required this.messaging,
    required this.chat,
    required this.userProfile,
  });

  @override
  _OrdersPageState createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  bool isLoading = false;
  bool hasMore = true;
  int documentLimit = 25;
  ScrollController _scrollController = ScrollController();
  DocumentSnapshot? lastDocument;
  late Query<Map<String, dynamic>> orderRef;
  List<DocumentSnapshot<Map<String, dynamic>>> items = [];
  late Query<Map<String, dynamic>> commandes;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    /*orderQuery();
    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      double delta = MediaQuery.of(context).size.height * 0.20;
      if (maxScroll - currentScroll <= delta) {
        orderQuery();
      }
    });*/

    if (widget.userRole == Fields.client) {
      setState(() {
        commandes = FirebaseFirestore.instance
            .collection(Fields.order)
            .where(Fields.userId, isEqualTo: widget.userId)
            .orderBy(Fields.orderDate, descending: true)
            .limit(200);
        //.orderBy(Fields.orderDate, descending: true);
      });
    } else {
      setState(() {
        commandes = FirebaseFirestore.instance
            .collection(Fields.order)
            .orderBy(Fields.orderDate, descending: true)
            .limit(200);
      });
    }
  }

  void orderQuery() {
    if (!hasMore) {
      return;
    }

    if (isLoading == true) {
      return;
    }

    if (mounted) {
      setState(() {
        isLoading = true;
      });
    }

    if (lastDocument == null) {
      if (widget.userRole == Fields.client) {
        orderRef = widget.db.databaseReference
            .collection(Fields.order)
            .where(Fields.userId, isEqualTo: widget.userId)
            .orderBy(Fields.status, descending: false)
            .limit(documentLimit);
      } else {
        orderRef = widget.db.databaseReference
            .collection(Fields.order)
            .orderBy(Fields.status, descending: false)
            .limit(documentLimit);
      }
    } else {
      if (widget.userRole == Fields.client) {
        orderRef = widget.db.databaseReference
            .collection(Fields.order)
            .where(Fields.userId, isEqualTo: widget.userId)
            .orderBy(Fields.status, descending: false)
            .startAfterDocument(lastDocument!)
            .limit(documentLimit);
      } else {
        orderRef = widget.db.databaseReference
            .collection(Fields.order)
            .orderBy(Fields.status, descending: false)
            .startAfterDocument(lastDocument!)
            .limit(documentLimit);
      }
    }

    orderRef.get().then((QuerySnapshot<Map<String, dynamic>> snapshot) {
      if (snapshot.docs.length < documentLimit) {
        hasMore = false;
      }

      if (snapshot.docs.length > 0)
        lastDocument = snapshot.docs[snapshot.docs.length - 1];

      if (mounted) {
        setState(() {
          for (int i = 0; i < snapshot.docs.length; i++) {
            Object? exist = items.firstWhereOrNull((Object element) {
              if (element is DocumentSnapshot<Map<String, dynamic>>) {
                bool isEqual = element.id == snapshot.docs[i].id;
                return isEqual;
              } else {
                return false;
              }
            });

            if (exist == null) {
              items.add(snapshot.docs[i]);
            }
          }

          isLoading = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ordersList(),
    );
  }

  Widget item(Order order) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.only(
            left: SizeConfig.diagonal * 0.2, right: SizeConfig.diagonal * 0.2),
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          elevation: 16,
          color: Colors.white.withOpacity(0.8),
          child: Container(
            alignment: Alignment.center,
            height: SizeConfig.diagonal * 10,
            width: SizeConfig.diagonal * 10,
            child: ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SingleOrderPage(
                      auth: widget.auth,
                      db: widget.db,
                      userId: widget.userId,
                      userRole: widget.userRole,
                      userName: widget.userName,
                      orderId: order.id!,
                      clientOrder: order,
                      messaging: widget.messaging,
                      chat: widget.chat,
                      userProfile: widget.userProfile,
                    ),
                  ),
                );
              },
              leading: Padding(
                padding: EdgeInsets.only(top: SizeConfig.diagonal * 1),
                child: Icon(
                  order.orderLocation == 0
                      ? FontAwesomeIcons.rectangleList
                      : order.orderLocation == 2
                          ? Icons.room_service
                          : FontAwesomeIcons.truckMoving,
                  size: 25,
                  color: Color(Styling.accentColor),
                ),
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 1,
                    child: ZText(
                      content: '${orderStatus(context, order)}',
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      color: colorPicker(order.status),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: ZText(
                      content:
                          '${I18n.of(context).total} : ${formatNumber(order.grandTotal)} ${I18n.of(context).fbu}',
                      textAlign: TextAlign.right,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              subtitle: Padding(
                padding: EdgeInsets.only(top: SizeConfig.diagonal * 1),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 1,
                      child: ZText(
                        content: order.orderLocation == 0
                            ? '${I18n.of(context).tableNumber} : ${order.tableAdress}'
                            : order.orderLocation == 2
                                ? '${I18n.of(context).roomNumber} : ${order.tableAdress}'
                                : '${I18n.of(context).address} : ${order.tableAdress}',
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: ZText(
                        content: order.orderDate == null
                            ? ""
                            : '${widget.formatter.format(order.orderDate!.toDate())}',
                        textAlign: TextAlign.right,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Color colorPicker(int status) {
    Color textColor = Colors.black;
    if (status == Fields.pending)
      textColor = Colors.red;
    else if (status == Fields.confirmed)
      textColor = Colors.green;
    else if (status == Fields.preparation)
      textColor = Colors.orange;
    else if (status == Fields.served) textColor = Colors.blue;

    return textColor;
  }

  Widget ordersList() {
    return StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
      stream: commandes.snapshots(),
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> snapshot) {
        if (snapshot.data == null || snapshot.data!.docs.length <= 0) {
          return Center(
            child: ZText(
              content: I18n.of(context).orderPlaceholder,
              textAlign: TextAlign.center,
              color: Color(Styling.primaryColor),
              fontWeight: FontWeight.bold,
            ),
          );
        }

        /* return ResponsiveListScaffold.builder(
          scaffoldKey: _scaffoldKey,
          detailBuilder: (BuildContext context, int? index, bool tablet) {
            Order order = Order.buildObject(snapshot.data!.docs[index!]);
            return DetailsScreen(
                body: SingleOrderPage(
              auth: widget.auth,
              db: widget.db,
              userId: widget.userId,
              userRole: widget.userRole,
              userName: widget.userName,
              orderId: order.id!,
              clientOrder: order,
              messaging: widget.messaging,
              chat: widget.chat,
            ));
          },
          nullItems: const Center(child: CircularProgressIndicator()),
          emptyItems: const Center(child: Text("No Items Found")),
          itemCount: snapshot.data!.docs.length,
          itemBuilder: (BuildContext context, int index) {
            Order order = Order.buildObject(snapshot.data!.docs[index]);
            return item(order);
          },
          bottomNavigationBar: BottomAppBar(
            elevation: 0.0,
            child: IconButton(
              icon: const Icon(Icons.share),
              onPressed: () {},
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text("Snackbar!"),
                ),
              );
            },
            child: const Icon(Icons.add),
          ),
        );*/

        return ListView(
          children: snapshot.data!.docs
              .map((DocumentSnapshot<Map<String, dynamic>> document) {
            Order order = Order.buildObject(document);

            return item(order);
          }).toList(),
        );
      },
    );
  }

  /*Widget ordersList() {
    return ListView(
      controller: _scrollController,
      children: [
        items.length == 0
            ? Center(
                child: ZText(content: ""),
              )
            : Column(
                children: items
                    .map((DocumentSnapshot<Map<String, dynamic>> document) {
                  Order order = Order.buildObject(document);
                  return item(order);
                }).toList(),
              ),
        isLoading
            ? Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(SizeConfig.diagonal * 1),
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation(Color(Styling.accentColor)),
                  ),
                ),
              )
            : Container()
      ],
    );
  }*/
}
