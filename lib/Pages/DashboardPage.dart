import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:badges/badges.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:sunstone/Components/ZAppBar.dart';
import 'package:sunstone/Components/ZElevatedButton.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Helpers/Utils.dart';
import 'package:sunstone/Models/Call.dart';
import 'package:sunstone/Models/Chat.dart';
import 'package:sunstone/Models/Fields.dart';
import 'package:sunstone/Models/OrderItem.dart';
import 'package:sunstone/Models/UserProfile.dart';
import 'package:sunstone/Pages/AdminPage.dart';
import 'package:sunstone/Pages/ChatPage.dart';
import 'package:sunstone/Pages/LoginPage.dart';
import 'package:sunstone/Pages/MenuPage.dart';
import 'package:sunstone/Pages/OrdersPage.dart';
import 'package:sunstone/Pages/SingleChatPage.dart';
import 'package:sunstone/Pages/StatPage.dart';
import 'package:sunstone/Pages/StockPage.dart';
import 'package:sunstone/Pages/UserPage.dart';
import 'package:sunstone/Services/Authentication.dart';
import 'package:sunstone/Services/Database.dart';
import 'package:sunstone/Services/Messaging.dart';
import 'package:sidebarx/sidebarx.dart';
import 'package:sunstone/i18n.dart';
import 'package:url_launcher/url_launcher.dart';

import '../Components/ZText.dart';
import 'DisabledPage.dart';

class DashboardPage extends StatefulWidget {
  final Authentication auth;
  final Database db;
  final String userId;
  final String userRole;
  final String userName;
  final Messaging messaging;
  final List<OrderItem>? clientOrder;
  final Chat? chat;
  final int? index;
  final UserProfile userProfile;

  DashboardPage({
    required this.auth,
    required this.userId,
    required this.userRole,
    required this.userName,
    required this.db,
    this.clientOrder,
    required this.messaging,
    this.index,
    required this.chat,
    required this.userProfile,
  });

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  int _selectedIndex = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int enabled = 1;
  double _xOffset0 = 0;
  double _xOffset1 = 0;
  double _xOffset2 = 0;
  var calls = FirebaseFirestore.instance
      .collection(Fields.calls)
      .orderBy(Fields.createdAt, descending: true)
      .limit(3);
  bool isActive = true;

  BuildContext? dialogContext;
  bool isStarting = true;
  bool isMenuExtended = true;
  late SidebarXController _menuController;
  int unreadCustomerCount = 0;
  int unreadAdminCount = 0;

  late StreamSubscription<DocumentSnapshot<Map<String, dynamic>>> configStream;
  late StreamSubscription<QuerySnapshot<Map<String, dynamic>>> unreadStream;
  late StreamSubscription<DocumentSnapshot<Map<String, dynamic>>> userStream;

  @override
  void dispose() {
    configStream.cancel();
    unreadStream.cancel();
    userStream.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _menuController = SidebarXController(selectedIndex: 0, extended: true);

    widget.db.updateLastSeen(widget.userId);

    if (widget.index != null) {
      if (mounted)
        setState(() {
          _selectedIndex = widget.index!;
        });
    }

    configStream = FirebaseFirestore.instance
        .collection(Fields.configuration)
        .doc(Fields.settings)
        .snapshots()
        .listen((DocumentSnapshot<Map<String, dynamic>> documentSnapshot) {
      if (mounted)
        setState(() {
          enabled = documentSnapshot.data()![Fields.enabled];
        });
    });

    userStream = FirebaseFirestore.instance
        .collection(Fields.users)
        .doc(widget.userId)
        .snapshots()
        .listen((DocumentSnapshot<Map<String, dynamic>> documentSnapshot) {
      if (mounted)
        setState(() {
          isActive = documentSnapshot.data()![Fields.isActive];
        });
    });

    /* widget.messaging.listenMessage(
      context,
      widget.auth,
      widget.db,
      widget.userId,
      widget.userRole,
      widget.messaging,
    );*/

    if (widget.userRole == Fields.chef ||
        widget.userRole == Fields.chefBoissons ||
        widget.userRole == Fields.chefCuisine) {
      calls.snapshots().listen((snapshot) {
        if (snapshot.size > 0) {
          Call call = Call.buildObject(snapshot.docs[0]);
          if (call.hasCalled && isStarting == false) {
            callDialog(call);
          } else if (isStarting == true) {
            isStarting = false;
          } else {
            Navigator.pop(dialogContext!);
          }
        }
      });
    }

    if (widget.userRole == Fields.client) {
      unreadStream = widget.db.databaseReference
          .collection(Fields.chats)
          .where(Fields.customerId, isEqualTo: widget.userId)
          .snapshots()
          .listen((snapshot) {
        if (snapshot.docs.isNotEmpty) {
          int count = 0;
          count =
              snapshot.docs.first.data()[Fields.customerUnreadCount] + count;

          if (mounted)
            setState(() {
              unreadCustomerCount = count;
            });
        } else {
          if (mounted)
            setState(() {
              unreadCustomerCount = 0;
            });
        }
      });
    } else {
      unreadStream = widget.db.databaseReference
          .collection(Fields.chats)
          .where(Fields.adminUnreadCount, isGreaterThan: 0)
          .snapshots()
          .listen((snapshot) {
        if (snapshot.docs.isNotEmpty) {
          int count = 0;
          snapshot.docs.forEach((element) {
            count = element.data()[Fields.adminUnreadCount] + count;
          });

          if (mounted)
            setState(() {
              unreadAdminCount = count;
            });
        } else {
          if (mounted)
            setState(() {
              unreadAdminCount = 0;
            });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    _xOffset2 = SizeConfig.safeBlockHorizontal * 0;
    switch (_selectedIndex) {
      case 0:
        if (mounted)
          setState(() {
            _xOffset0 = SizeConfig.safeBlockHorizontal * 0;
            _xOffset1 = SizeConfig.safeBlockHorizontal * 200;
            _xOffset2 = SizeConfig.safeBlockHorizontal * 200;
          });
        break;
      case 1:
        if (mounted)
          setState(() {
            _xOffset0 = SizeConfig.safeBlockHorizontal * -200;
            _xOffset1 = SizeConfig.safeBlockHorizontal * 0;
            _xOffset2 = SizeConfig.safeBlockHorizontal * 200;
          });
        break;
      case 2:
        if (mounted)
          setState(() {
            _xOffset0 = SizeConfig.safeBlockHorizontal * -200;
            _xOffset1 = SizeConfig.safeBlockHorizontal * -200;
            _xOffset2 = SizeConfig.safeBlockHorizontal * 0;
          });
        break;
    }

    return enabled == 0
        ? DisabledPage(
            auth: widget.auth,
            db: widget.db,
            userId: widget.userId,
            userRole: widget.userRole,
          )
        : isActive == false
            ? DisabledPage(
                auth: widget.auth,
                db: widget.db,
                userId: widget.userId,
                userRole: widget.userRole,
              )
            : Container(
                // color: Color(Styling.primaryBackgroundColor).withOpacity(0.1),
                child: Scaffold(
                  key: _scaffoldKey,
                  backgroundColor: Colors.transparent,
                  drawer: widget.userRole == Fields.admin &&
                          deviceSize(context) == DeviceSize.mobile
                      ? sideMenu()
                      : null,
                  appBar: buildAppBar(
                    context,
                    widget.auth,
                    false,
                    logout,
                    null,
                    null,
                    widget.userRole == Fields.admin ? admin : null,
                    loadData: widget.userRole == Fields.admin ||
                            widget.userRole == Fields.developer
                        ? loadData
                        : null,
                    callStaff:
                        widget.userRole == Fields.client ? callStaff : null,
                  ),
                  body: deviceSize(context) == DeviceSize.mobile
                      ? smallScreenBody()
                      : largeScreenBody(),
                  bottomNavigationBar: widget.userRole == Fields.admin
                      ? null
                      : CurvedNavigationBar(
                          animationCurve: Curves.easeInBack,
                          //color: Colors.white.withOpacity(0.7),
                          height: 50,
                          //height: SizeConfig.diagonal * 6,
                          animationDuration: Duration(milliseconds: 800),

                          //backgroundColor: Colors.transparent,
                          backgroundColor: Color(Styling.grey),
                          index: _selectedIndex,
                          items: <Widget>[
                            Badge(
                              badgeContent: ZText(
                                content: widget.userRole == Fields.client
                                    ? unreadCustomerCount > 99
                                        ? "99+"
                                        : "$unreadCustomerCount"
                                    : unreadAdminCount > 99
                                        ? "99+"
                                        : "$unreadAdminCount",
                                fontSize: 10,
                                color: Colors.white,
                              ),
                              badgeColor: Color(Styling.accentColor),
                              child: Icon(
                                Icons.chat,
                                color: Color(Styling.primaryColor),
                              ),
                            ),
                            Icon(
                              Icons.restaurant_menu_outlined,
                              color: Color(Styling.primaryColor),
                            ),
                            Icon(
                              Icons.shopping_bag,
                              color: Color(Styling.primaryColor),
                            ),
                          ],

                          onTap: (index) {
                            if (mounted)
                              setState(() {
                                _selectedIndex = index;
                              });
                          },
                        ),
                ),
              );
  }

  void homeDrawer() {
    if (_scaffoldKey.currentState!.isDrawerOpen) {
      _scaffoldKey.currentState!.closeDrawer();
    } else {
      _scaffoldKey.currentState!.openDrawer();
    }
  }

  void callStaff() async {
    final Uri uri = Uri(
      scheme: 'tel',
      path: widget.db.callNumber,
    );
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    }
  }

  void loadData() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.custom,
      allowedExtensions: ['csv'],
    );

    if (result != null) {
      EasyLoading.show(status: I18n.of(context).loading);

      bool isOnline = await hasConnection();
      if (!isOnline) {
        EasyLoading.dismiss();

        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: ZText(content: I18n.of(context).noInternet),
          ),
        );
      } else {
        try {
          //List<PlatformFile> files = result.paths.map((path) => PlatformFile(path!)).toList();
          PlatformFile? menu;
          PlatformFile? category;

          for (int i = 0; i < result.files.length; i++) {
            PlatformFile platformFile = result.files[i];
            log(platformFile.name);
            if (platformFile.name == 'menu.csv') {
              menu = platformFile;
            }

            if (platformFile.name == 'category.csv') {
              category = platformFile;
            }
          }

          if (menu == null || category == null) {
            EasyLoading.dismiss();

            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: ZText(content: "menu or category null"),
              ),
            );
          } else {
            await widget.db.sendData(menu, category);
            EasyLoading.dismiss();
          }
        } on Exception catch (e) {
          //print('Error: $e');
          EasyLoading.dismiss();

          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: ZText(content: e.toString()),
            ),
          );
        }
      }
    } else {
      // User canceled the picker
    }
  }

  void callDialog(Call call) {
    showGeneralDialog(
      context: context,
      barrierDismissible: false,
      transitionDuration: Duration(milliseconds: 300),
      transitionBuilder: (context, a1, a2, widget) {
        dialogContext = context;
        return Transform.scale(
          scale: a1.value,
          child: Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ZText(
                    content:
                        "${I18n.of(context).tableNumber} ${call.order.tableAdress} ${I18n.of(context).called}",
                  ),
                  Icon(
                    Icons.warning,
                    size: SizeConfig.diagonal * 15,
                    color: Colors.red,
                  ),
                  ZElevatedButton(
                    onpressed: () => updateCall(call),
                    topPadding: 0.0,
                    bottomPadding: 0.0,
                    child: ZText(
                      content:
                          "${I18n.of(context).accept} ${I18n.of(context).tableNumber} ${call.order.tableAdress}",
                    ),
                  )
                ],
              ),
              height: SizeConfig.diagonal * 45,
            ),
          ),
        );
      },
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return Container();
      },
    );
  }

  Future<void> updateCall(Call call) async {
    EasyLoading.show(status: I18n.of(context).loading);
    bool isOnline = await hasConnection();
    if (!isOnline) {
      EasyLoading.dismiss();

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: ZText(content: I18n.of(context).noInternet),
      ));
    } else {
      try {
        await widget.db.updateCall(call, false);
        EasyLoading.dismiss();
        // Navigator.of(context).pop();
      } on Exception catch (e) {
        EasyLoading.dismiss();

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: ZText(content: e.toString()),
        ));
      }
    }
  }

  Widget largeScreenBody() {
    return Row(
      children: [
        if (widget.userRole == Fields.admin) sideMenu(),
        Expanded(
          child: widget.userRole == Fields.admin
              ? mainContentAdmin()
              : mainContent(),
        ),
      ],
    );
  }

  Widget smallScreenBody() {
    return Container(
      child:
          widget.userRole == Fields.admin ? mainContentAdmin() : mainContent(),
    );
  }

  Widget sideMenu() {
    return Card(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: SidebarX(
        controller: _menuController,
        theme: SidebarXTheme(
          margin: EdgeInsets.all(SizeConfig.diagonal * 1),
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(10),
          ),
          textStyle: const TextStyle(color: Color(Styling.textColor)),
          selectedTextStyle: const TextStyle(color: Color(Styling.textColor)),
          itemTextPadding: EdgeInsets.only(left: SizeConfig.diagonal * 2),
          selectedItemTextPadding:
              EdgeInsets.only(left: SizeConfig.diagonal * 2),
          selectedItemDecoration: BoxDecoration(
            color: Color(Styling.accentColor),
          ),
          iconTheme: IconThemeData(
            color: Color(Styling.primaryColor),
            size: 20,
          ),
        ),
        extendedTheme: SidebarXTheme(
          width: 200,
          decoration: BoxDecoration(
            color: Colors.transparent,
          ),
          margin: EdgeInsets.only(right: SizeConfig.diagonal * 1),
        ),
        footerDivider: Divider(color: Colors.white.withOpacity(0.3), height: 1),
        headerBuilder: (context, extended) {
          return SizedBox(
            height: 100,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Image.asset('assets/logo.png'),
            ),
          );
        },
        items: [
          if (widget.userRole == Fields.admin)
            SidebarXItem(
              icon: Icons.dashboard,
              label: I18n.of(context).dashboard,
              onTap: () {
                if (mounted)
                  setState(() {
                    _selectedIndex = 0;
                  });
              },
            ),
          SidebarXItem(
            // icon: Icons.chat,
            iconWidget: Badge(
              badgeContent: ZText(
                content: widget.userRole == Fields.client
                    ? unreadCustomerCount > 99
                        ? "99+"
                        : "$unreadCustomerCount"
                    : unreadAdminCount > 99
                        ? "99+"
                        : "$unreadAdminCount",
                fontSize: 10,
                color: Colors.white,
              ),
              badgeColor: Color(Styling.accentColor),
              child: Icon(
                Icons.chat,
                color: Color(Styling.primaryColor),
              ),
            ),
            label: I18n.of(context).chat,
            onTap: () {
              if (mounted)
                setState(() {
                  _selectedIndex = 1;
                });
            },
          ),
          SidebarXItem(
            icon: Icons.restaurant_menu_outlined,
            label: I18n.of(context).menu,
            onTap: () {
              if (mounted)
                setState(() {
                  _selectedIndex = 2;
                });
            },
          ),
          if (widget.userRole == Fields.admin)
            SidebarXItem(
              icon: Icons.fastfood,
              label: I18n.of(context).stock,
              onTap: () {
                if (mounted)
                  setState(() {
                    _selectedIndex = 3;
                  });
              },
            ),
          SidebarXItem(
            icon: Icons.shopping_bag,
            label: I18n.of(context).orders,
            onTap: () {
              if (mounted)
                setState(() {
                  _selectedIndex = 4;
                });
            },
          ),
          if (widget.userRole == Fields.admin)
            SidebarXItem(
              icon: Icons.supervised_user_circle,
              label: I18n.of(context).users,
              onTap: () {
                if (mounted)
                  setState(() {
                    _selectedIndex = 5;
                  });
              },
            ),
        ],
      ),
    );
  }

  Widget mainContentAdmin() {
    switch (_selectedIndex) {
      case 0:
        return StatPage(
          auth: widget.auth,
          db: widget.db,
          userId: widget.userId,
          userRole: widget.userRole,
        );
      case 1:
        if (widget.userRole == Fields.admin) {
          return ChatPage(
            auth: widget.auth,
            db: widget.db,
            userId: widget.userId,
            userRole: widget.userRole,
          );
        } else {
          Chat chat;
          if (widget.chat == null) {
            chat = Chat(
                id: null,
                customerId: widget.userId,
                customerName: widget.userName,
                adminUnreadCount: 0,
                customerUnreadCount: 0);
          } else {
            chat = widget.chat!;
          }
          return SingleChatPage(
            auth: widget.auth,
            db: widget.db,
            userId: widget.userId,
            userRole: widget.userRole,
            chat: chat,
            hasBackButton: false,
          );
        }
      case 2:
        return MenuPage(
          auth: widget.auth,
          db: widget.db,
          userId: widget.userId,
          userRole: widget.userRole,
          clientOrder: widget.clientOrder,
          messaging: widget.messaging,
          userName: widget.userName,
          chat: widget.chat,
          userProfile: widget.userProfile,
        );
      case 3:
        return StockPage(
          auth: widget.auth,
          db: widget.db,
          messaging: widget.messaging,
          userId: widget.userId,
          userRole: widget.userRole,
        );
      case 4:
        return OrdersPage(
          auth: widget.auth,
          db: widget.db,
          userId: widget.userId,
          userRole: widget.userRole,
          messaging: widget.messaging,
          userName: widget.userName,
          chat: widget.chat,
          userProfile: widget.userProfile,
        );
      case 5:
        return UserPage(
          db: widget.db,
          auth: widget.auth,
          userRole: widget.userRole,
          userId: widget.userId,
        );
      default:
        return StatPage(
          auth: widget.auth,
          db: widget.db,
          userId: widget.userId,
          userRole: widget.userRole,
        );
    }
  }

  Widget mainContent() {
    Chat chat;
    if (widget.chat == null) {
      chat = Chat(
          id: null,
          customerId: widget.userId,
          customerName: widget.userName,
          adminUnreadCount: 0,
          customerUnreadCount: 0);
    } else {
      chat = widget.chat!;
    }
    return Stack(
      children: [
        AnimatedContainer(
          child: SingleChatPage(
            auth: widget.auth,
            db: widget.db,
            userId: widget.userId,
            userRole: widget.userRole,
            chat: chat,
            hasBackButton: false,
          ),
          curve: Curves.easeInBack,
          duration: Duration(milliseconds: 800),
          transform: Matrix4.translationValues(_xOffset0, 0, 1),
        ),
        AnimatedContainer(
          child: MenuPage(
            auth: widget.auth,
            db: widget.db,
            userId: widget.userId,
            userRole: widget.userRole,
            clientOrder: widget.clientOrder,
            messaging: widget.messaging,
            userName: widget.userName,
            chat: widget.chat,
            userProfile: widget.userProfile,
          ),
          curve: Curves.easeInBack,
          duration: Duration(milliseconds: 800),
          transform: Matrix4.translationValues(_xOffset1, 0, 1),
        ),
        AnimatedContainer(
          child: OrdersPage(
            auth: widget.auth,
            db: widget.db,
            userId: widget.userId,
            userRole: widget.userRole,
            messaging: widget.messaging,
            userName: widget.userName,
            chat: widget.chat,
            userProfile: widget.userProfile,
          ),
          curve: Curves.easeInBack,
          duration: Duration(milliseconds: 800),
          transform: Matrix4.translationValues(_xOffset2, 0, 1),
        )
      ],
    );
  }

  void admin() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => AdminPage(
                auth: widget.auth,
                db: widget.db,
                messaging: widget.messaging,
                userId: widget.userId,
                userRole: widget.userRole,
              )),
    );
  }

  void logout() async {
    try {
      configStream.cancel();
      unreadStream.cancel();
      userStream.cancel();
      unawaited(widget.auth.signOut());
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => LoginPage(
                  auth: widget.auth,
                  db: widget.db,
                  messaging: widget.messaging,
                )),
      );
    } on Exception catch (e) {
      print('Error: $e');
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: ZText(content: e.toString()),
        ),
      );
    }
  }
}
