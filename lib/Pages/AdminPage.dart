import 'package:flutter/material.dart';
import 'package:sunstone/Components/ZAppBar.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:sunstone/Helpers/SizeConfig.dart';
import 'package:sunstone/Helpers/Styling.dart';
import 'package:sunstone/Services/Database.dart';
import 'package:sunstone/Services/Messaging.dart';
import 'package:sunstone/Pages/StatPage.dart';
import 'package:sunstone/Pages/StockPage.dart';
import 'package:sunstone/Pages/UserPage.dart';
import 'package:sunstone/Services/Authentication.dart';

class AdminPage extends StatefulWidget {
  final Authentication auth;
  final Database db;
  final Messaging messaging;
  final String userId;
  final String userRole;

  AdminPage({
    required this.auth,
    required this.db,
    required this.userId,
    required this.userRole,
    required this.messaging,
  });

  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  int _pageState = 0;
  double _xoffset1 = 0;
  double _xoffset2 = 0;
  double _xoffset3 = 0;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    _xoffset1 = SizeConfig.safeBlockHorizontal * 0;

    switch (_pageState) {
      case 0:
        if (mounted)
          setState(() {
            _xoffset1 = SizeConfig.safeBlockHorizontal * 0;
            _xoffset2 = SizeConfig.safeBlockHorizontal * 200;
            _xoffset3 = SizeConfig.safeBlockHorizontal * 200;
          });
        break;
      case 1:
        if (mounted)
          setState(() {
            _xoffset1 = SizeConfig.safeBlockHorizontal * -200;
            _xoffset2 = SizeConfig.safeBlockHorizontal * 0;
            _xoffset3 = SizeConfig.safeBlockHorizontal * 200;
          });
        break;
      case 2:
        if (mounted)
          setState(() {
            _xoffset1 = SizeConfig.safeBlockHorizontal * -200;
            _xoffset2 = SizeConfig.safeBlockHorizontal * -200;
            _xoffset3 = SizeConfig.safeBlockHorizontal * 0;
          });
    }

    return Scaffold(
      appBar: buildAppBar(
        context,
        widget.auth,
        true,
        null,
        null,
        null,
        null,
      ),
      body: body(),
      backgroundColor: Colors.transparent,
      bottomNavigationBar: CurvedNavigationBar(
        animationCurve: Curves.easeInBack,
        //color: Colors.white.withOpacity(0.7),
        backgroundColor: Color(Styling.grey),
        height: 54,
        //backgroundColor: Colors.transparent,
        animationDuration: Duration(milliseconds: 800),
        index: _pageState,
        items: <Widget>[
          Icon(
            Icons.stacked_bar_chart,
            color: Color(Styling.primaryColor),
          ),
          Icon(
            Icons.fastfood,
            color: Color(Styling.primaryColor),
          ),
          Icon(
            Icons.supervised_user_circle,
            color: Color(Styling.primaryColor),
          ),
        ],
        onTap: (index) {
          if (mounted)
            setState(() {
              _pageState = index;
            });
        },
      ),
    );
  }

  Widget body() {
    return Stack(
      children: [
        AnimatedContainer(
          child: StatPage(
            auth: widget.auth,
            db: widget.db,
            userId: widget.userId,
            userRole: widget.userRole,
          ),
          duration: Duration(milliseconds: 800),
          curve: Curves.easeInBack,
          transform: Matrix4.translationValues(_xoffset1, 0, 1),
        ),
        AnimatedContainer(
          child: StockPage(
            auth: widget.auth,
            db: widget.db,
            messaging: widget.messaging,
            userId: widget.userId,
            userRole: widget.userRole,
          ),
          duration: Duration(milliseconds: 800),
          curve: Curves.easeInBack,
          transform: Matrix4.translationValues(_xoffset2, 0, 1),
        ),
        AnimatedContainer(
          child: UserPage(
            db: widget.db,
            auth: widget.auth,
            userRole: widget.userRole,
            userId: widget.userId,
          ),
          duration: Duration(milliseconds: 800),
          curve: Curves.easeInBack,
          transform: Matrix4.translationValues(_xoffset3, 0, 1),
        ),
      ],
    );
  }
}
