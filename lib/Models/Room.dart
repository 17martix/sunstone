import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'Fields.dart';

class Room {
  late String id;
  late String name;
  late bool? isOccupied;

  Room({
    required this.id,
    required this.name,
    this.isOccupied,
  });

  Room.buildObject(DocumentSnapshot<Map<String, dynamic>> document) {
    id = document.data()![Fields.id];
    name = document.data()![Fields.name];
    isOccupied = document.data()![Fields.isOccupied] ?? false;
  }

  Room.buildObjectAsync(
      AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>> document) {
    id = document.data?[Fields.id];
    name = document.data![Fields.name];
    isOccupied = document.data![Fields.isOccupied] ?? false;
  }
}
