import 'package:flutter/material.dart';
import 'package:sunstone/Models/Fields.dart';
import 'package:sunstone/i18n.dart';

class Result {
  bool isSuccess;
  String message;

  Result({
    required this.isSuccess,
    required this.message,
  });

   String getMessage(BuildContext context) {
    if (message == Fields.operationFailed)
      return I18n.of(context).operationFailed;
    else if (message == Fields.operationSucceeded)
      return I18n.of(context).operationSucceeded;
    else if (message == Fields.operationTooLong)
      return I18n.of(context).timeout;
    else
      return I18n.of(context).operationFailed;
  }
}
