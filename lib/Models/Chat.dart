import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sunstone/Models/Fields.dart';

class Chat {
  late String? id;
  late String customerName;
  late String customerId;

  late num adminUnreadCount;
  late num customerUnreadCount;
  late Timestamp? createdAt;
  late Timestamp? updatedAt;

  Chat({
    this.id,
    required this.customerName,
    required this.customerId,
    this.createdAt,
    this.updatedAt,
    required this.customerUnreadCount,
    required this.adminUnreadCount,
  });

  Chat.buildObject(DocumentSnapshot<Map<String, dynamic>> document) {
    id = document.id;
    customerName = document.data()![Fields.customerName];

    customerId = document.data()![Fields.customerId];

    createdAt = document.data()![Fields.createdAt];

    updatedAt = document.data()![Fields.updatedAt];

    customerUnreadCount = document.data()![Fields.customerUnreadCount];
    adminUnreadCount = document.data()![Fields.adminUnreadCount];
  }

  Chat.buildObjectAsync(AsyncSnapshot<DocumentSnapshot> document) {
    id = document.data!.id;
    customerName = document.data![Fields.customerName];

    customerId = document.data![Fields.customerId];

    createdAt = document.data![Fields.createdAt];

    updatedAt = document.data![Fields.updatedAt];

    customerUnreadCount = document.data![Fields.customerUnreadCount];
    adminUnreadCount = document.data![Fields.adminUnreadCount];
  }
}
