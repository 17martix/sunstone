class Fields {
  static final String users = "users";
  static final String configuration = "configuration";
  static final String order = "order";
  static final String items = "items";

  static final String id = "id";
  static final String role = "role";
  static final String receiveNotifications = "receiveNotifications";
  static final String isActive = "isActive";

  static final String client = 'client';
  static final String chef = 'chef';
  static final String server = 'server';
  static final String admin = 'admin';
  static final String developer = 'developer';

  static final String chefCuisine = 'chefCuisine';
  static final String chefBoissons = 'chefBoissons';
  static final String statisticUser = 'statisticUser';
  static final String statisticStock = 'statisticStock';

  static final String taxes = 'taxes';
  static final String percentage = 'percentage';

  static final String orderLocation = 'orderLocation';
  static final String instructions = 'instructions';
  static final String grandTotal = 'grandTotal';
  static final String status = 'status';
  static final String orderDate = 'orderDate';
  static final String confirmedDate = 'confirmedDate';
  static final String preparationDate = 'preparationDate';
  static final String servedDate = 'servedDate';
  static final String userId = 'userId';
  static final String userRole = 'userRole';
  static final String taxPercentage = 'taxPercentage';
  static final String total = 'total';

  static final String image = 'image';
  static final String file = 'file';
  static final String video = 'video';
  static final String text = 'text';

  static final String userName = 'userName';

  static final String count = 'count';
  static final String name = 'name';
  static final String category = 'category';
  static final String price = 'price';
  static final String rank = 'rank';
  static final String global = 'global';

  static final String menu = 'menu';
  static final String createdAt = 'createdAt';
  static final String availability = 'availability';

  static final int pending = 1;
  static final int confirmed = 2;
  static final int preparation = 3;
  static final int served = 4;

  static final String tableAdress = 'tableAdress';
  static final String phoneNumber = 'phoneNumber';
  static final String tout = 'Tout';
  static final String boissonslegeres = 'Soft drinks';

  static final String settings = 'settings';
  static final String enabled = 'enabled';
  static final String imageName = 'imageName';
  static final String calls = 'calls';

  static final String hasCalled = 'hasCalled';

  static final String token = 'token';
  static final String lastSeenAt = 'lastSeenAt';

  static final String orderId = 'orderId';
  static final String addressName = 'addressName';
  static final String geoPoint = 'geoPoint';
  static final String addresses = 'addresses';
  static final String address = 'address';
  static final String currentPoint = 'currentPoint';
  static final String typedAddress = 'typedAddress';
  static final String deliveringOrderId = 'deliveringOrderId';
  static final String isDrink = 'isDrink';
  static final String date = 'date';
  static final String statistic = 'statistic';

  static final String quantity = 'quantity';
  static final String unit = 'unit';
  static final String usedSince = 'usedSince';
  static final String usedTotal = 'usedTotal';
  static final String stock = 'stock';

  static final String itemId = 'itemId';
  static final String itemName = 'itemName';
  static final String substQuantity = 'substQuantity';
  static final String linked = 'linked';
  static final String condiments = 'condiments';
  static final String tags = 'tags';

  static final String stockId = 'stockId';
  static final String refillStock = 'refillStock';
  static final String updateStock = 'updateStock';
  static final String useImages = 'useImages';
  static final String isRestaurantRoomHome = 'isRestaurantRoomHome';
  static final String imageUrl = 'imageUrl';
  static final String description = 'description';

  static final String chats = 'chats';
  static final String bothUserId = 'bothUserId';
  static final String updatedAt = 'updatedAt';
  static final String customerName = 'customerName';
  static final String customerId = 'customerId';
  static final String customerUnreadCount = 'customerUnreadCount';
  static final String adminUnreadCount = 'adminUnreadCount';

  static final String operationFailed = 'operationFailed';
  static final String operationSucceeded = 'operationSucceeded';
  static final String operationTooLong = 'operationTooLong';
  static final String conversations = 'conversations';
  static final String activeNotifications = 'activeNotifications';

  static final String message = 'message';
  static final String writtenAt = 'writtenAt';
  static final String writtenById = 'writtenById';
  static final String writtenToId = 'writtenToId';
  static final String chatId = 'chatId';
  static final String type = 'type';
  static final String url = 'url';
  static final String fileName = 'fileName';
  static final String encryptedMessage = 'encryptedMessage';
  static final String localTime = 'localTime';
  static final String notSentConersations = 'notSentConersations';

  static final String encryptedText = 'encryptedText';
  static final String messageKey = "TeQrG^7w#@%^!&YkZwegdt%@&^%!#Yg>";

  static final String call = 'call';

  static final String isOccupied = 'isOccupied';
  static final String rooms = 'rooms';
  static final String assignedRoom = 'assignedRoom';
}
