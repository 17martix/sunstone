import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sunstone/Models/Fields.dart';

class Conversation {
  late String? id;
  late String message;
  late Timestamp? writtenAt;
  late String writtenById;
  late String writtenToId;
  late String? chatId;
  late int localTime;

  late String? type; //image or url or video or file or emoji or text
  late String? url;
  late String? fileName;

  late String? encryptedMessage;

  Conversation({
    this.id,
    required this.message,
    this.writtenAt,
    required this.writtenById,
    required this.writtenToId,
    required this.chatId,
    required this.localTime,
    this.type,
    this.url,
    this.fileName,
    this.encryptedMessage,
  });

  Conversation.buildObjectAsync(
      AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>> document) {
    id = document.data!.id;
    message = document.data![Fields.message];
    writtenAt = document.data![Fields.writtenAt];
    writtenById = document.data![Fields.writtenById];
    writtenToId = document.data![Fields.writtenToId];
    chatId = document.data![Fields.chatId];
    type = document.data?[Fields.type];
    url = document.data?[Fields.url];
    fileName = document.data?[Fields.fileName];
    encryptedMessage = document.data?[Fields.encryptedMessage];
  }

  Conversation.buildObject(DocumentSnapshot<Map<String, dynamic>> document) {
    id = document.id;
    message = document.data()![Fields.message];
    writtenAt = document.data()![Fields.writtenAt];
    writtenById = document.data()![Fields.writtenById];
    writtenToId = document.data()![Fields.writtenToId];
    chatId = document.data()![Fields.chatId];
    localTime = document.data()![Fields.localTime];
    type = document.data()?[Fields.type];
    url = document.data()?[Fields.url];
    fileName = document.data()?[Fields.fileName];
    encryptedMessage = document.data()?[Fields.encryptedMessage];
  }

  String buildStringFromObject() {
    String text =
        "$id#;*;#$message#;*;#$writtenById#;*;#$writtenToId#;*;#$chatId#;*;#$localTime#;*;#$type#;*;#$url#;*;#$fileName#;*;#$encryptedMessage";
    return text;
  }

  Conversation.buildObjectFromString(String text) {
    List<String> list = text.split("#;*;#");
    id = list[0];
    message = list[1];
    writtenAt = null;
    writtenById = list[2];
    writtenToId = list[3];
    chatId = list[4];
    localTime = int.parse(list[5]);
    if (list[5] == "null")
      type = null;
    else
      type = list[6];
    if (list[6] == "null")
      url = null;
    else
      url = list[7];
    if (list[7] == "null")
      fileName = null;
    else
      fileName = list[8];

    if (list[9] == "null")
      encryptedMessage = null;
    else
      encryptedMessage = list[9];
  }
}
