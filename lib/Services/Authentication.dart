import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';

class Authentication {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<String?> anonymousSignIn() async {
    UserCredential result = await _firebaseAuth.signInAnonymously();
    User? user = result.user;
    return user?.uid;
  }

  Future<User?> getCurrentUser() async {
    //User? user = await _firebaseAuth.currentUser;
    User? user = await FirebaseAuth.instance.authStateChanges().first;
    return user;
  }

  Future<void> signOut() async {
    return await _firebaseAuth.signOut();
  }

  FirebaseAuth getAuth() {
    return _firebaseAuth;
  }

  Future<User?> registerWithEmail(String email, String password) async {
    UserCredential result = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);

    return result.user;
  }

  Future<User?> signInWithEmail(String email, String password) async {
    UserCredential result = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);

    return result.user;
  }
}
