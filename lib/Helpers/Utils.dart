import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:extended_image/extended_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart' hide MenuItem;
import 'package:flutter/services.dart';
import 'package:sunstone/Models/Cart.dart';
import 'package:sunstone/Models/Category.dart';
import 'package:sunstone/Models/Fields.dart';
import 'package:sunstone/Models/MenuItem.dart';
import 'package:sunstone/Models/Order.dart';
import 'package:sunstone/Models/OrderItem.dart';

import '../i18n.dart';

enum DeviceSize { tablet, mobile, computer }

DeviceSize deviceSize(BuildContext context) {
  double width = MediaQuery.of(context).size.width;
  if (width < 720) {
    //600
    return DeviceSize.mobile;
  } else if (width < 1280) {
    //1444
    return DeviceSize.tablet;
  } else {
    return DeviceSize.computer;
  }
}

Widget imageStateHandler(ExtendedImageState state) {
  switch (state.extendedImageLoadState) {
    case LoadState.loading:
      return Image.asset(
        "assets/loading.gif",
        fit: BoxFit.fill,
      );
      break;

    ///if you don't want override completed widget
    ///please return null or state.completedWidget
    //return null;
    //return state.completedWidget;
    case LoadState.completed:
      return state.completedWidget;
      // _controller.forward();
      /*return FadeTransition(
        // opacity: _controller,
        child: ExtendedRawImage(
          image: state.extendedImageInfo?.image,
          width: ScreenUtil.instance.setWidth(600),
          height: ScreenUtil.instance.setWidth(400),
        ),
      );*/
      break;
    case LoadState.failed:
      /* return const SizedBox(
        width: double.infinity,
        height: double.infinity,
      );*/
      return GestureDetector(
        child: Image.asset(
          "assets/logo.png",
          fit: BoxFit.fill,
        ),
        onTap: () {
          state.reLoadImage();
        },
      );
      break;
  }
}

OrderItem findOrderItem(List<OrderItem> list, String id) {
  return list.firstWhere((element) => element.menuItem.id == id);
}

bool isAlreadyOnTheOrder(List<OrderItem> list, String id) {
  try {
    findOrderItem(list, id);
    return true;
  } catch (e) {
    return false;
  }
}

String numberItems(context, List<OrderItem> order) {
  Cart cart = cartCount(order);
  if (cart.totalCount == 1)
    return "${cart.totalCount} ${I18n.of(context).item}";
  else
    return "${cart.totalCount} ${I18n.of(context).items}";
}

String priceItems(context, List<OrderItem> order) {
  Cart cart = cartCount(order);
  return "${I18n.of(context).total} : ${formatNumber(cart.totalPrice)} ${I18n.of(context).fbu}";
}

String priceItemsTotal(context, List<OrderItem> order) {
  Cart cart = cartCount(order);
  return "${formatNumber(cart.totalPrice)} ${I18n.of(context).fbu}";
}

int priceItemsTotalNumber(context, List<OrderItem> order) {
  Cart cart = cartCount(order);
  return cart.totalPrice;
}

String grandTotal(context, List<OrderItem> order, int taxPercentage) {
  Cart cart = cartCount(order);
  double tax = (taxPercentage / 100) * cart.totalPrice;
  double total = cart.totalPrice + tax;
  int roundedTotal = total.round();
  return "${formatNumber(roundedTotal)} ${I18n.of(context).fbu}";
}

double grandTotalNumber(context, List<OrderItem> order, int taxPercentage) {
  Cart cart = cartCount(order);
  double tax = (taxPercentage / 100) * cart.totalPrice;
  double total = cart.totalPrice + tax;
  return total;
}

Cart cartCount(List<OrderItem> order) {
  int totalCount = 0;
  int totalPrice = 0;
  for (int i = 0; i < order.length; i++) {
    totalCount += order[i].count;
    totalPrice += (order[i].menuItem.price * order[i].count).toInt();
  }

  return new Cart(totalCount: totalCount, totalPrice: totalPrice);
}

String appliedTax(context, List<OrderItem> order, int taxPercentage) {
  Cart cart = cartCount(order);
  double tax = (taxPercentage / 100) * cart.totalPrice;
  int roundedTax = tax.round();
  return "$taxPercentage% = ${formatNumber(roundedTax)} ${I18n.of(context).fbu}";
}

String appliedTaxFromTotal(context, int total, int taxPercentage) {
  double tax = (taxPercentage / 100) * total;
  int roundedTax = tax.round();
  return "$taxPercentage% = ${formatNumber(roundedTax)} ${I18n.of(context).fbu}";
}

String? orderCardTtle(context, Order order) {
  int orderLength = order.clientOrder.length;
  String? text;
  if (orderLength == 1) {
    text = "${order.clientOrder[0].menuItem.name}";
  } else if (orderLength == 2) {
    text =
        "${order.clientOrder[0].menuItem.name} ${I18n.of(context).and} ${order.clientOrder.length} ${I18n.of(context).moreItems}";
  } else if (orderLength > 2) {
    text =
        "${order.clientOrder[0].menuItem.name} ${I18n.of(context).and} ${order.clientOrder.length} ${I18n.of(context).moreItems}";
  }

  return text;
}

String? orderStatus(context, Order order) {
  String? text;
  if (order.status == Fields.pending) {
    text = "${I18n.of(context).pendingOrder}";
  } else if (order.status == Fields.confirmed) {
    text = "${I18n.of(context).confirmedOrder}";
  } else if (order.status == Fields.preparation) {
    text = "${I18n.of(context).orderPreparation}";
  } else if (order.status == Fields.served) {
    text = "${I18n.of(context).orderServed}";
  }

  return text;
}

List<String> getUserTags(String name, String phoneNumber) {
  List<String> searchIndex = [];

  searchIndex.addAll(createTags(name));
  searchIndex.addAll(createTags(phoneNumber));

  return searchIndex;
}

List<String> createTags(String text) {
  text = text.trim();
  text = text.toLowerCase();
  text = text.replaceAll(new RegExp(r'(?:_|[^\w\s])+'), '');
  List<String> list = [];
  String s = '';
  List<String> splitList = text.split(" ");

  for (int i = 0; i < splitList.length; i++) {
    list.add(splitList[i]);
    if (s == '') s = splitList[i];

    if (i > 0) {
      for (int j = i; j < splitList.length; j++) {
        s += " " + splitList[j];
        list.add(s);
      }
      s = splitList[i];
    }
  }

  //for (int i = 0; i < list.length; i++) print(list[i]);

  return list;
}

String itemTotal(context, OrderItem orderItem) {
  int total = orderItem.menuItem.price * orderItem.count;
  return "$total ${I18n.of(context).fbu}";
}

String itemTax(context, Order order) {
  var value = order.total * order.taxPercentage / 100;
  return "${order.taxPercentage}% = $value ${I18n.of(context).fbu}";
}

String? showRommTable(context, Order order) {
  String? text;
  if (order.orderLocation == 0)
    text = "${I18n.of(context).tableNumber} : ${order.tableAdress}";
  else if (order.orderLocation == 2)
    text = "${I18n.of(context).roomNumber} : ${order.tableAdress}";
  else if (order.orderLocation == 1)
    text = "${I18n.of(context).addr} : ${order.tableAdress}";
  return text;
}

String capitalize(String text) {
  text = text.trim();
  if (text.length > 0) {
    String firstLetter = text[0];
    firstLetter = firstLetter.toUpperCase();
    String remaining = text.substring(1);
    remaining = remaining.toLowerCase();
    return firstLetter + remaining;
  }
  return text;
}

Future<List<MenuItem>> getMenuItems() async {
  List<MenuItem> list = [];

  /*await new HttpClient()
      .getUrl( Uri.parse('http://foo.bar/foo.txt'))
      .then((HttpClientRequest request) async => await request.close())
      .then((HttpClientResponse response) async =>
          await response.pipe(new File('assets/menu.csv').openWrite()));*/

  /*File file = File('assets/menu.csv');
  list = file.readAsLinesSync().skip(1) // Skip the header row
      .map((line) {
    final parts = line.split(',');
    return MenuItem(
      name: parts[0],
      price: int.tryParse(parts[1]),
      category: parts[2],
      availability: int.tryParse(parts[3]),
      rank: int.tryParse(parts[4]),
      global: int.tryParse(parts[5]),
      imageName: parts[6],
    );
  }).toList();*/

  String text = await rootBundle.loadString('assets/menu.csv');
  list = LineSplitter.split(text).map((line) {
    final parts = line.split(',');
    return MenuItem(
      name: capitalize(parts[0]),
      price: int.parse(parts[1]),
      category: capitalize(parts[2]),
      availability: int.parse(parts[3]),
      rank: int.parse(parts[4]),
      global: int.parse(parts[5]),
      imageName: parts[6],
      description: parts[8].isEmpty ? null : capitalize(parts[8]),
    );
  }).toList();

  return list;
}

Future<List<MenuItem>> getMenuItemsFromFile(PlatformFile file) async {
  List<MenuItem> list = [];

  /* list = file.readAsLinesSync().skip(1) // Skip the header row
      .map((line) {
    final parts = line.split(',');
    //log("${parts[0]} ${parts[1]} ${parts[2]} ${parts[3]}");

    return MenuItem(
      name: capitalize(parts[0]),
      price: int.parse(parts[1]),
      category: capitalize(parts[2]),
      availability: int.parse(parts[3]),
      rank: int.parse(parts[4]),
      global: int.parse(parts[5]),
      imageName: parts[6],
      isDrink: int.tryParse(parts[7]),
    );
  }).toList();*/

  String fileContent = utf8.decode(file.bytes!);
  List<String> lines = fileContent.split('\n');

  lines.skip(1).forEach((line) {
    final parts = line.split(',');
    if (parts[0].isNotEmpty &&
        parts[1].isNotEmpty &&
        parts[2].isNotEmpty &&
        parts[3].isNotEmpty &&
        parts[4].isNotEmpty &&
        parts[5].isNotEmpty &&
        parts[6].isNotEmpty &&
        parts[7].isNotEmpty) {
      MenuItem menu = MenuItem(
        name: capitalize(parts[0]),
        price: int.parse(parts[1]),
        category: capitalize(parts[2]),
        availability: int.parse(parts[3]),
        rank: int.parse(parts[4]),
        global: int.parse(parts[5]),
        imageName: parts[6],
        isDrink: int.tryParse(parts[7]),
        description: parts[8].isEmpty ? null : capitalize(parts[8]),
      );
      list.add(menu);
    }
  });

  return list;
}

Future<List<Category>> getCategoryListFromFile(PlatformFile file) async {
  List<Category> list = [];

  /* list = file.readAsLinesSync().skip(1) // Skip the header row
      .map((line) {
    final parts = line.split(',');
    return Category(
      name: capitalize(parts[0]),
      rank: int.parse(parts[1]),
      imageName: parts[2],
    );
  }).toList();*/

  String fileContent = utf8.decode(file.bytes!);
  List<String> lines = fileContent.split('\n');

  lines.skip(1).forEach((line) {
    final parts = line.split(',');
    if (parts[0].isNotEmpty && parts[1].isNotEmpty && parts[2].isNotEmpty) {
      Category category = Category(
        name: capitalize(parts[0]),
        rank: int.parse(parts[1]),
        imageName: parts[2].trim(),
      );
      list.add(category);
    }
  });

  return list;
}

Future<List<Category>> getCategoryList() async {
  List<Category> list = [];

  /*await new HttpClient()
      .getUrl( Uri.parse('http://foo.bar/foo.txt'))
      .then((HttpClientRequest request) async => await request.close())
      .then((HttpClientResponse response) async =>
          await response.pipe(new File('assets/category.csv').openWrite()));*/

  /*File file = File('assets/category.csv');
  list = file.readAsLinesSync().skip(1) // Skip the header row
      .map((line) {
    final parts = line.split(',');
    return Category(
      name: parts[0],
      rank: int.tryParse(parts[1]),
      imageName: parts[2],
    );
  }).toList();*/

  String text = await rootBundle.loadString('assets/category.csv');
  list = LineSplitter.split(text).map((line) {
    final parts = line.split(',');
    return Category(
      name: capitalize(parts[0]),
      rank: int.parse(parts[1]),
      imageName: parts[2],
    );
  }).toList();

  return list;
}

String formatNumber(num unformatedNumber) {
  int number = unformatedNumber.toInt();
  String oldNumber = "$number";
  String newNumber = '';
  for (int i = oldNumber.length - 1; i >= 0; i--) {
    newNumber = oldNumber[i] + newNumber;
    if (i != oldNumber.length - 1 && i != 0) {
      int remaining = (oldNumber.length - i) % 3;
      if (remaining == 0) {
        newNumber = ' ' + newNumber;
      }
    }
  }
  return newNumber;
}

Future<bool> hasConnection() async {
  if (kIsWeb) {
    return true;
  } else {
    try {
      final result = await InternetAddress.lookup('google.com')
          .timeout(const Duration(seconds: 10));
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    } catch (e) {
      return false;
    }
    return false;
  }
}

String? getMapsKey() {
  String? key;
  if (Platform.isAndroid) {
    key = "AIzaSyCgqCs92H-qHsydK12T--PtHgalQQ4M2Ds";
  } else if (Platform.isIOS) {
    key = "AIzaSyDf6dcsPsK9ku6lx96faPTz34_zPaxyvwE";
  }

  return key;
}

String? formatInterVal(double number) {
  String? text;

  int a = number.round();
  String textNumber = a.toString();
  int length = textNumber.length;

  if (length < 4) {
    text = textNumber;
  } else if (length < 7) {
    text = textNumber.substring(0, length - 3) + 'k';
  } else if (length < 10) {
    text =
        '${textNumber.substring(0, length - 6)}.${textNumber.substring(1, length - 5)} M';
  } else {
    text = textNumber;
  }
  return text;
}

String searchReady(String text) {
  text = text.trim();
  text = text.toLowerCase();
  text = text.replaceAll(new RegExp(r'(?:_|[^\w\s])+'), '');
  return text;
}

String? commandePluriel(num count, context) {
  String? commande;
  if (count == 1) {
    commande = I18n.of(context).order;
  } else {
    commande = I18n.of(context).orders;
  }
  return commande;
}

Color colorsStatStock(int index) {
  late Color colorStock;

  if (index == 0) {
    colorStock = Colors.blue;
  } else if (index == 1) {
    colorStock = Colors.yellow;
  } else if (index == 2) {
    colorStock = Colors.purpleAccent;
  } else if (index == 3) {
    colorStock = Colors.lightGreen;
  } else if (index == 4) {
    colorStock = Colors.pink;
  } else {
    colorStock = Colors.black;
  }

  return colorStock;
}

List<String> getMenuTags(MenuItem menuItem) {
  List<String> searchIndex = [];

  searchIndex.addAll(createTags(menuItem.name));
  searchIndex.addAll(createTags(menuItem.category!));
  return searchIndex;
}

String? emailValidator(context, String? value) {
  if (value == null) return I18n.of(context).requiredInput;
  value = value.trim();
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern.toString());
  if (value.isEmpty) return I18n.of(context).requiredInput;
  if (!regex.hasMatch(value)) return I18n.of(context).invalidEmail;

  return null;
}

String? passwordValidator(context, String? value) {
  if (value == null) return I18n.of(context).requiredInput;
  if (value.isEmpty) return I18n.of(context).requiredInput;
  if (value.length < 6) return I18n.of(context).passwordShort;

  return null;
}

String? cPasswordValidator(context, String? value, String? pass) {
  if (value == null) return I18n.of(context).requiredInput;
  if (value.isEmpty) return I18n.of(context).requiredInput;
  if (value != pass) return I18n.of(context).passwordUnmatch;
  if (value.length < 6) return I18n.of(context).passwordShort;

  return null;
}

String selectedRoomName(String value, BuildContext context) {
  if (value == "noRoomAssigned") return I18n.of(context).noRoomAssigned;
  if (value == "ambre") return "Ambre";
  if (value == "amethiste") return "Amethiste";
  if (value == "christale") return "Christale";
  if (value == "jade") return "Jade";
  if (value == "nomad") return "Nomad";
  if (value == "rubis") return "Rubis";
  if (value == "sapphire") return "Sapphire";
  if (value == "sunset") return "Sunset";
  if (value == "tente1") return "Tente 1";
  if (value == "tente2") return "Tente 2";
  if (value == "tente3") return "Tente 3";
  if (value == "vip") return "Sunstone Vip";
  return "";
}
