class Styling {
  static const int primaryBackgroundColor = 0xffFFFFFF;
  static const int secondaryBackgroundColor = 0xffB2EBF2;
  static const int primaryColor = 0xff0077BD;
  static const int primaryDarkColor = 0xffFFFFFF;
  static const int textColor = 0xff000000;
  static const int transparentBackground = 0xff8000bfff;
  static const int transparentBackgroundDark = 0xff80000000;
  static const int iconColor = 0xff0077BD;
  static const int accentColor = 0xffED7743;
  static const int grey = 0xffEEEEEE; //text color
  static const int lightGrey = 0xffFAFAFA; //text color

  static const int canvasColor = 0xff0077BD;
}
